package parametros;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Utilitario.Excel;

public class TpCyberCreditPromesasPago extends Excel{
	public static String nombreArchivo = "C:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-GestionesBitacoras.xlsx";
	
	private int nroCorrelativo;
	private String cliente;
	private String cuenta;
	private String monto;
	private String fecha;
	private String area;
	private String telefono;
	private String extension;
	private String outMensaje;
	
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getMonto() {
		return monto;
	}
	public void setMonto(String monto) {
		this.monto = monto;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getOutMensaje() {
		return outMensaje;
	}
	public void setOutMensaje(String outMensaje) {
		this.outMensaje = outMensaje;
	}
	
	public static ArrayList<TpCyberCreditPromesasPago> leerExcel() throws IOException{
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<TpCyberCreditPromesasPago> arrayObject = new ArrayList<TpCyberCreditPromesasPago>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TpCyberCreditPromesasPago tcParameters = new TpCyberCreditPromesasPago();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCliente(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setCuenta(formatter.formatCellValue(row.getCell(2)));
			tcParameters.setMonto(formatter.formatCellValue(row.getCell(3)));
			tcParameters.setFecha(formatter.formatCellValue(row.getCell(4)));
			tcParameters.setArea(formatter.formatCellValue(row.getCell(5)));
			tcParameters.setTelefono(formatter.formatCellValue(row.getCell(6)));
			tcParameters.setExtension(formatter.formatCellValue(row.getCell(7)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}
