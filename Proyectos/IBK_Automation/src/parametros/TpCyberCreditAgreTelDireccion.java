package parametros;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Utilitario.Excel;

public class TpCyberCreditAgreTelDireccion extends Excel{
	public static String nombreArchivo = "C:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-AgregarTelefonoDireccion.xlsx";
	
	private int nroCorrelativo;
	private String cliente;
	private String cuenta;
	private String tipo;
	private String area;
	private String numero;
	private String extension;
	private String tipoDireccion;
	private String Direccion;
	private String Distrito;
	private String Provincia;
	private String DepartamentoCorto;
	private String cp;
	
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String cliente) {
		this.cliente = cliente;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getArea() {
		return area;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}
	public String getTipoDireccion() {
		return tipoDireccion;
	}
	public void setTipoDireccion(String tipoDireccion) {
		this.tipoDireccion = tipoDireccion;
	}
	public String getDireccion() {
		return Direccion;
	}
	public void setDireccion(String direccion) {
		Direccion = direccion;
	}
	public String getDistrito() {
		return Distrito;
	}
	public void setDistrito(String distrito) {
		Distrito = distrito;
	}
	public String getProvincia() {
		return Provincia;
	}
	public void setProvincia(String provincia) {
		Provincia = provincia;
	}
	public String getDepartamentoCorto() {
		return DepartamentoCorto;
	}
	public void setDepartamentoCorto(String departamentoCorto) {
		DepartamentoCorto = departamentoCorto;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public static ArrayList<TpCyberCreditAgreTelDireccion> leerExcel() throws IOException{
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<TpCyberCreditAgreTelDireccion> arrayObject = new ArrayList<TpCyberCreditAgreTelDireccion>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TpCyberCreditAgreTelDireccion tcParameters = new TpCyberCreditAgreTelDireccion();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCliente(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setCuenta(formatter.formatCellValue(row.getCell(2)));
			tcParameters.setTipo(formatter.formatCellValue(row.getCell(3)));
			tcParameters.setArea(formatter.formatCellValue(row.getCell(4)));
			tcParameters.setNumero(formatter.formatCellValue(row.getCell(5)));
			tcParameters.setExtension(formatter.formatCellValue(row.getCell(6)));
			tcParameters.setTipoDireccion(formatter.formatCellValue(row.getCell(7)));
			tcParameters.setDireccion(formatter.formatCellValue(row.getCell(8)));
			tcParameters.setDistrito(formatter.formatCellValue(row.getCell(9)));
			tcParameters.setProvincia(formatter.formatCellValue(row.getCell(10)));
			tcParameters.setDepartamentoCorto(formatter.formatCellValue(row.getCell(11)));
			tcParameters.setCp(formatter.formatCellValue(row.getCell(12)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}
