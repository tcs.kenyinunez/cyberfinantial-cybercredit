package parametros;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Utilitario.Excel;

public class TpComprobarStatus extends Excel{
	public static String nombreArchivo = "";
	
	private int nroCorrelativo;
	private String cuenta;
	private String cliente;
	private String nombre;
	private String direccion;
	private String mensaje;
	
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getCliente() {
		return cliente;
	}
	public void setCliente(String moneda) {
		this.cliente = moneda;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String importe) {
		this.nombre = importe;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String fecha) {
		this.direccion = fecha;
	}
	
	public static ArrayList<TpComprobarStatus> leerExcel() throws IOException{
		Properties properties = new Properties();
		properties.load(new FileReader(".\\properties\\general.properties"));
		nombreArchivo = properties.getProperty("Creditos");
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<TpComprobarStatus> arrayObject = new ArrayList<TpComprobarStatus>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TpComprobarStatus tcParameters = new TpComprobarStatus();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCuenta(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setCliente(formatter.formatCellValue(row.getCell(2)));
			tcParameters.setNombre(formatter.formatCellValue(row.getCell(3)));
			tcParameters.setDireccion(formatter.formatCellValue(row.getCell(4)));
			tcParameters.setMensaje(formatter.formatCellValue(row.getCell(6)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}
