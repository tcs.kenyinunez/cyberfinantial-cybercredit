package parametros;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Utilitario.Excel;

public class TpObtenerCoutaInicial extends Excel{
	public static String nombreArchivo = "C:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-Creditos.xlsx";
	
	private int nroCorrelativo;
	private String cuenta;
	private String grupo;
	private String subcuenta;
	private String montoVencido;
	
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getMontoVencido() {
		return montoVencido;
	}
	public void setMontoVencido(String montoVencido) {
		this.montoVencido = montoVencido;
	}
	public String getSubcuenta() {
		return subcuenta;
	}
	public void setSubcuenta(String subcuenta) {
		this.subcuenta = subcuenta;
	}
	public static ArrayList<TpObtenerCoutaInicial> leerExcel() throws IOException{
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<TpObtenerCoutaInicial> arrayObject = new ArrayList<TpObtenerCoutaInicial>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TpObtenerCoutaInicial tcParameters = new TpObtenerCoutaInicial();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setCuenta(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setGrupo(formatter.formatCellValue(row.getCell(2)));
			tcParameters.setSubcuenta(formatter.formatCellValue(row.getCell(3)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}
