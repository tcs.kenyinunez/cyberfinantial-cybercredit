package parametros;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import Utilitario.Excel;

public class TpPagoCouta extends Excel{
	private Properties properties = new Properties();
	public static String nombreArchivo = "";
	
	private int nroCorrelativo;
	private String nroCuenta;
	private String Importe;
	private String Moneda;
	private String Fecha;
	private String Tipo;
	private String Motivo;
	private String MedioCargo;
	private String estado;
	
	public String getMotivo() {
		return Motivo;
	}
	public void setMotivo(String motivo) {
		Motivo = motivo;
	}
	public String getMedioCargo() {
		return MedioCargo;
	}
	public void setMedioCargo(String medioCargo) {
		MedioCargo = medioCargo;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public int getNroCorrelativo() {
		return nroCorrelativo;
	}
	public void setNroCorrelativo(int nroCorrelativo) {
		this.nroCorrelativo = nroCorrelativo;
	}
	public String getNroCuenta() {
		return nroCuenta;
	}
	public void setNroCuenta(String nroContrato) {
		this.nroCuenta = nroContrato;
	}
	public String getImporte() {
		return Importe;
	}
	public void setImporte(String importe) {
		Importe = importe;
	}
	public String getMoneda() {
		return Moneda;
	}
	public void setMoneda(String moneda) {
		Moneda = moneda;
	}
	public String getFecha() {
		return Fecha;
	}
	public void setFecha(String fecha) {
		Fecha = fecha;
	}
	public String getTipo() {
		return Tipo;
	}
	public void setTipo(String tipo) {
		Tipo = tipo;
	}
	public static ArrayList<TpPagoCouta> leerExcel() throws IOException{
		Properties properties = new Properties();
		properties.load(new FileReader(".\\properties\\general.properties"));
		nombreArchivo = properties.getProperty("PagoCouta");
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		int rowCount = ws.getLastRowNum();
		System.out.println("Se encontraron " + (rowCount -4) + " registros");
		ArrayList<TpPagoCouta> arrayObject = new ArrayList<TpPagoCouta>();
		for(int indexRow=5;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TpPagoCouta tcParameters = new TpPagoCouta();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setNroCorrelativo(Integer.parseInt(formatter.formatCellValue(row.getCell(0))));
			tcParameters.setNroCuenta(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setMoneda(formatter.formatCellValue(row.getCell(3)));
			tcParameters.setImporte(formatter.formatCellValue(row.getCell(4)));
			tcParameters.setFecha(formatter.formatCellValue(row.getCell(5)));
			tcParameters.setTipo(formatter.formatCellValue(row.getCell(6)));
			tcParameters.setMotivo(formatter.formatCellValue(row.getCell(7)));
			tcParameters.setMedioCargo(formatter.formatCellValue(row.getCell(8)));
			tcParameters.setMensaje(formatter.formatCellValue(row.getCell(9)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public static void establecerValorCelda(int indRow, int indCell, String data) throws IOException {
		fi= new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Hoja1");
		row=ws.getRow(indRow);
		cell=row.createCell(indCell);
		cell.setCellValue(data);
		fo=new FileOutputStream(nombreArchivo);
		wb.write(fo);		
		wb.close();
		fi.close();
		fo.close();
	}
}
