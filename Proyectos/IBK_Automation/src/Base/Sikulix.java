package Base;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import autoitx4java.AutoItX;

public class Sikulix {
	public Screen screen = new Screen();
	public Pattern element;
	public String rutaImagen = "";
	public Selenium accion;
	public int xInicio = 0;
	public int yInicio = 0;
	public int xFinal = 0;
	public int xMedio = 0;
	public int yMedio = 0;
	public Match m;
	public int longitudCelda=0;
	public int alturaCelda = 0;

	public Sikulix(String rutaImagen) {
		this.rutaImagen = rutaImagen;
		element = new Pattern(rutaImagen);
		accion = new Selenium();
	}

	/**
	 * Evalua si elemento se encuentra en la pantalla
	 * 
	 * @param rutaImage {Ruta de la imagen que se buscar� en la pantalla}
	 * @return boolean {true - en caso se encontr� el elemento} {false - en caso
	 *         contrario}
	 * @throws InterruptedException
	 */
	public boolean exists() {
		if (screen.exists(element.similar(0.5f)) == null) {
			System.out.println("El elemento con ruta : " + this.rutaImagen + " no se encontr�");
			return false;
		}
		return true;
	}

	/**
	 * Espera por 10 segundos el elemento
	 * 
	 * @param rutaImage
	 */
	public void wait(String rutaImage) {
		try {
			screen.wait(element, 10);
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha encontrado el elemento " + this.rutaImagen);
		}
	}
	public void waitInvisibility() throws Exception {
		try {
			int seconds = 0;
			while(this.exists()) {
				this.hover();
				Thread.sleep(1000);
				seconds++;
				if(seconds==30) {
					throw new Exception("Se espero 30 segundos a que el elemento " + this.rutaImagen + " desapareciera" );
				}
			}
			return;
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}
	/**
	 * Espera por 30 segundos el elemento
	 * 
	 * @param rutaImage
	 */
	public void waitAtImage() {
		try {
			int x= 0;
			while(!this.exists()) {
				Thread.sleep(1000);
				x++;
				if(x==30) {
					throw new FindFailed("");
				}
			}
//			screen.wait(element.similar(0.8f), 1);
			
			this.getCoordsImagen();
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println("No se ha encontrado el elemento " + this.rutaImagen);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void clearText() throws InterruptedException {
		this.click();
		AutoIt auto = new AutoIt();
		auto.getAutoIt().send("{HOME}{END}{BACKSPACE}",false);
		Thread.sleep(1000);
	}
	/**
	 * Hace click y escribe en el elemento
	 * 
	 * @param keys {Cadena que se desea escribir}
	 * @throws InterruptedException 
	 */
	public void sendKeys(String keys) throws InterruptedException {
		Thread.sleep(1000);
		this.click();
		Thread.sleep(1000);
		screen.type(keys);
	}
	/**
	 * Hace click en el elemento que se encuentre en la pantalla
	 * 
	 * @param rutaImage
	 */
	public void click() {
		element = new Pattern(rutaImagen);
		if (this.exists()) {
			try {
				this.getCoordsImagen();
				screen.click(element);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	public void hover() {
		element = new Pattern(rutaImagen);
		if(this.exists()) {
			this.getCoordsImagen();
			screen.hover();
		}
	}
	public void getCoordsImagen() {
		try {
			m = screen.find(element);
			xInicio = m.getTopLeft().getX();
			yInicio = m.getTopLeft().getY();
			xFinal = m.getTopRight().getX();
			xMedio = m.getCenter().getX();
			yMedio = m.getCenter().getY();
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void resizeElement(int pixelLeft) throws FindFailed {
		this.getCoordsImagen();
		AutoIt auto =new AutoIt();
		auto.getAutoIt().mouseClickDrag("left", xFinal, yMedio, xFinal + pixelLeft, yMedio);
	}
	public void captureSizeElement() throws InterruptedException, FindFailed, AWTException, IOException {
		this.getCoordsImagen();
		Thread.sleep(1000);
		longitudCelda = (m.getCenter().getX() - m.getTopLeft().getX()) * 2;
		alturaCelda = (m.getCenter().getY() - m.getTopLeft().getY()) * 2;
		int times = 2;
	}
	
	public String captureCell(int xInicio,int yInicio,int xFinal, int yFinal,int index) throws AWTException, IOException {
		Robot robot = new Robot();
		Rectangle rectanguloFoto = new Rectangle(xInicio,yInicio,xFinal,yFinal);
		System.out.println(rectanguloFoto.toString());
		BufferedImage screenShot = robot
				.createScreenCapture(rectanguloFoto);
		ImageIO.write(screenShot,"png",new File(".\\Resources\\cell" + index + ".png"));
		return ".\\Resources\\cell" + index + ".png";
	}
	
	/**
	 * Maximiza una ventana mediante atajos de teclado
	 * @throws FindFailed
	 * @throws InterruptedException
	 * @throws AWTException
	 */
	
	// MAXIMIZAR VENTANA
	public void Maximizar() throws FindFailed, InterruptedException, AWTException {
		Thread.sleep(2000);
//		accion.ClickAImagen("Resources\\Maximizar.png");
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_WINDOWS);
		robot.keyPress(KeyEvent.VK_UP);
		robot.keyRelease(KeyEvent.VK_UP);
		robot.keyRelease(KeyEvent.VK_WINDOWS);
//		AutoIt auto = new AutoIt();
//		auto.getAutoIt().winSetState("http://s417va7:9080/bei010Web/bei010/bei10.jsp - Internet Explorer","",AutoItX.SW_MAXIMIZE);
	}
	
	@Override
	public String toString() {
		return "Sikulix [screen=" + screen + ", element=" + element + ", rutaImagen=" + rutaImagen + ", accion="
				+ accion + ", xInicio=" + xInicio + ", yInicio=" + yInicio + ", xFinal=" + xFinal + ", xMedio=" + xMedio
				+ ", yMedio=" + yMedio + ", m=" + m + ", longitudCelda=" + longitudCelda + ", alturaCelda="
				+ alturaCelda + "]";
	}
}
