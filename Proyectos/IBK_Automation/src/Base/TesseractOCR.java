package Base;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

import Pruebas.Web.ObtenerCuotaIncialPrueba;


public class TesseractOCR implements Runnable {
	private static final TesseractOCR Read_File = null;
	public static String strCuenta;
	public static String strCuota;
	public static String strImporte;
	public static String strEstado;

	public static String getTextImage(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\"");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strCuenta = Read_File.read_a_file(output_file + ".txt");
			
			String numCero = "0";
			String BlankSpace = "";
			strCuenta=strCuenta.replace("O", numCero);
			strCuenta=strCuenta.replace(" ", BlankSpace);
			System.out.println("Esta es la cuenta capturada: " + strCuenta);
			return strCuenta;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	
	public static String getTextImageOnlyNumber(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\" -psm 6");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strCuenta = Read_File.read_a_file(output_file + ".txt");
			
			String numCero = "0";
			String BlankSpace = "";
			strCuenta=strCuenta.replace("O", numCero);
			strCuenta=strCuenta.replace(" ", BlankSpace);
			System.out.println("Esta es la cuenta capturada: " + strCuenta);
			return strCuenta;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	public static String getTextImageOnlyLetters(String copyPath) {
		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;
		try {
			p = Runtime.getRuntime().exec(command);
			new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
			new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
			PrintWriter stdin = new PrintWriter(p.getOutputStream());
			stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\" nobatch letters");	
			stdin.close();
			p.waitFor();
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println();
			
			strCuenta = Read_File.read_a_file(output_file + ".txt");
			
			String numCero = "0";
			String BlankSpace = "";
			strCuenta=strCuenta.replace("O", numCero);
			strCuenta=strCuenta.replace(" ", BlankSpace);
			System.out.println("Esta es la cuenta capturada: " + strCuenta);
			return strCuenta;

		} catch (Exception e) {
			e.printStackTrace();
			return "";
		}
	}
	public void Cuota(String copyPathCuota) throws IOException, InterruptedException {
		ObtenerCuotaIncialPrueba cyber = new ObtenerCuotaIncialPrueba();
		String input_file1 = copyPathCuota;
		String output_file1 = ".\\Resources\\out";
		String tesseract_install_path1 = "C:\\Program Files (x86)\\Tesseract-OCR\\tesseract";
		String[] command1 = { "cmd", };
		Process p1;

		p1 = Runtime.getRuntime().exec(command1);
		new Thread(new SyncPipe(p1.getErrorStream(), System.err)).start();
		new Thread(new SyncPipe(p1.getInputStream(), System.out)).start();
		PrintWriter stdin1 = new PrintWriter(p1.getOutputStream());
		stdin1.println("\"" + tesseract_install_path1 + "\" \"" + input_file1 + "\" \"" + output_file1 + "\"");
		stdin1.close();
		p1.waitFor();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		strCuota = Read_File.read_a_file(output_file1 + ".txt");
		
		FileReader fr = new FileReader(".\\Resources\\out.txt");
		BufferedReader bf = new BufferedReader(fr);
		
		long lNumeroLineas = 0;
		 
		while ((strCuota = bf.readLine())!=null) {
		  lNumeroLineas++;
		}
			
			
		System.out.println("Cantidad de registros de cuotas: " + lNumeroLineas);

	}

	public void Cuenta(String copyPath) throws IOException, InterruptedException {

		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;

		p = Runtime.getRuntime().exec(command);
		new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
		new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
		PrintWriter stdin = new PrintWriter(p.getOutputStream());
		stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\"");
		stdin.close();
		p.waitFor();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		strCuenta = Read_File.read_a_file(output_file + ".txt");

		String numCero = "0";
		String BlankSpace = "";
		strCuenta = strCuenta.replace("O", numCero);
		strCuenta = strCuenta.replace(" ", BlankSpace);
		System.out.println("Esta es la cuenta capturada: " + strCuenta);

	}
	
	public void Pago(String copyPath) throws IOException, InterruptedException {

		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;

		p = Runtime.getRuntime().exec(command);
		new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
		new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
		PrintWriter stdin = new PrintWriter(p.getOutputStream());
		stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\"");
		stdin.close();
		p.waitFor();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		strImporte = Read_File.read_a_file(output_file + ".txt");

		System.out.println("Esta es el importe capturado: " + strImporte);

	}

	public static String read_a_file(String file_name) {
		BufferedReader br = null;
		String read_string = "";
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader(file_name));
			while ((sCurrentLine = br.readLine()) != null) {
				read_string = read_string + sCurrentLine;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return read_string;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

	}

	public void Estado(String copyPath) throws IOException, InterruptedException {
		// TODO Auto-generated method stub
		String input_file = copyPath;
		String output_file = ".\\Resources\\out";
		String tesseract_install_path = "D:\\Programs\\Tesseract-OCR\\tesseract.exe";
		String[] command = { "cmd", };
		Process p;

		p = Runtime.getRuntime().exec(command);
		new Thread(new SyncPipe(p.getErrorStream(), System.err)).start();
		new Thread(new SyncPipe(p.getInputStream(), System.out)).start();
		PrintWriter stdin = new PrintWriter(p.getOutputStream());
		stdin.println("\"" + tesseract_install_path + "\" \"" + input_file + "\" \"" + output_file + "\"");
		stdin.close();
		p.waitFor();
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

	    strEstado = Read_File.read_a_file(output_file + ".txt");
		String BlankSpace = "";
		strEstado = strEstado.replace("S", BlankSpace);
		strEstado = strEstado.replace("t", BlankSpace);
		strEstado = strEstado.replace("a", BlankSpace);
		strEstado = strEstado.replace("u", BlankSpace);
		strEstado = strEstado.replace("s", BlankSpace);
		strEstado = strEstado.replace(":", BlankSpace);
		strEstado = strEstado.replace("5", BlankSpace);
		strCuenta = strCuenta.replace("", BlankSpace);
		System.out.println("Esta es la cuenta capturada: " + strEstado);
	}

}
