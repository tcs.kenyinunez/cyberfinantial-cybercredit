package Base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

import Utilitario.Conecci�n.InitSelenium;

public class Selenium {
	private WebDriver driver;

	public Selenium() {
		driver = InitSelenium.getDriver();
	}

	// MOVIMIENTO DEL MOUSE
	public void hoverByAction(WebElement element) {
		Actions actions = new Actions(driver);
		actions.moveToElement(element).perform();
	}

	// VALIDADOR DEL ELEMENTO WEB
	public boolean isElementPresent2(String xpathOfElement) {
		try {
			driver.switchTo().defaultContent();
			boolean x = driver.findElements(By.xpath(xpathOfElement)).size() > 0;
			if (!x) {
				return false;
			} else {
				return true;
			}

		} catch (Exception e) {
			return false;
		}
	}

	// OBTENER TITULO DE LA VENTANA
	public String ObtenerHandleAtual() {
		String handle = driver.getWindowHandle();
		String titulo = driver.switchTo().window(handle).getTitle();
		System.out.println("-----------titulo: " + titulo);
		return handle;
	}

	// FINALIZAR APLICACI�N
	public void FinalizarAplicacion() throws InterruptedException, IOException {
		driver.close();
		driver.quit();
		FinalizarProceso("IEDriverServer.exe");
	}

	// FINALIZAR PROCESO
	public void FinalizarProceso(String Proceso) {
		// Intentamos finalizar el prooceso indicado.
		try {

			String line;
			String pidInfo = "";

			Process p = Runtime.getRuntime().exec("C:\\Windows\\System32\\tasklist.exe");

			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));

			while ((line = input.readLine()) != null) {
				pidInfo += line;
			}

			input.close();

			if (pidInfo.contains(Proceso)) {
				System.out.println("Finalizando proceso: " + Proceso);
				Runtime.getRuntime().exec("taskkill /F /IM " + Proceso);
				System.out.println("Proceso finalizado: " + Proceso);
			}

		}

		// En caso de no poder finalizar el proceso devolvemos un mensaje de error.
		catch (IOException e) {
			System.out.println("Hubo un problema al intentar finalizar el proceso");
			e.printStackTrace();
		}
	}

	// LEER ELEMENTO WEB
	public String LeerElemento(String tipoElemento, String rutaElemento) {

		tipoElemento = tipoElemento.toLowerCase();
		String Resultado = "";
		// Ubicamos el elemendo con el resource id y hacemos click.
		if (tipoElemento.equals("id")) {
			Resultado = driver.findElement(By.id(rutaElemento)).getText();
			if (Resultado.equals("")) {
				Resultado = driver.findElement(By.id(rutaElemento)).getAttribute("value");
			}
		}

		// Ubicamos el elemendo con el resource xpath y hacemos click.
		if (tipoElemento.equals("xpath")) {
			Resultado = driver.findElement(By.xpath(rutaElemento)).getText();

			if (Resultado.equals("")) {
				Resultado = driver.findElement(By.xpath(rutaElemento)).getAttribute("value");
			}
		}
		// Obtenemos el texto del elemento indicado.
		// Imprimimos y devolvemos el resultado.
		System.out.println("Lectura: " + Resultado);
		return Resultado;
	}

	// COMANDOS DE SIKULIX
	public void ClickAImagen(String rutaImagen) throws InterruptedException, FindFailed {
		Thread.sleep(1000); 
		Screen s = new Screen();
		Pattern inicioButton = new Pattern(rutaImagen);

		if (s.exists(inicioButton) != null) {
			s.click(inicioButton);
		}
		Thread.sleep(500);
	}

	public void DoubleClickAImagen(String rutaImagen) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern inicioButton = new Pattern(rutaImagen);

		if (s.exists(inicioButton) != null) {
			s.doubleClick(inicioButton);
		}
		Thread.sleep(500);
	}

	public void SendKeysAImagen(String rutaImagen, String strUsuario) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern textfield = new Pattern(rutaImagen);
		System.out.println(s.wait(textfield,1));
		if (s.exists(textfield) != null) {
			s.click(textfield);
			s.type(strUsuario);
		}
		Thread.sleep(500);
	}
	
	public void MoveAImagen(String rutaImagen) throws InterruptedException, FindFailed {
		Thread.sleep(1000);
		Screen s = new Screen();
		Pattern inicioButton = new Pattern(rutaImagen);

		if (s.exists(inicioButton) != null) {
			//s.click(inicioButton);
		}
		Thread.sleep(500);
	}
	// POSICIONARSE EN VENTANA ACTUAL
	public void IrAVentana(String tituloVentana) {

		for (String winHandle : driver.getWindowHandles()) {
			if (driver.switchTo().window(winHandle).getTitle().equals(tituloVentana)) {
				driver.switchTo().window(winHandle);
				System.out.println("CAMBIANDO FOCO A VENTANA:" + tituloVentana);
				break;
			}
		}
	}
	
	//AMPLIAR IMAGEN
	//Ancho m�ximo
    public static int MAX_WIDTH=800;
    //Alto m�ximo
    public static int MAX_HEIGHT=800;
     
    public static void copyImage(String filePath, String copyPath) {
        BufferedImage bimage = loadImage(filePath);
        if(bimage.getHeight()>bimage.getWidth()){
            int heigt = (bimage.getHeight() * MAX_WIDTH) / bimage.getWidth();
            bimage = resize(bimage, MAX_WIDTH, heigt);
            int width = (bimage.getWidth() * MAX_HEIGHT) / bimage.getHeight();
            bimage = resize(bimage, width, MAX_HEIGHT);
        }else{
            int width = (bimage.getWidth() * MAX_HEIGHT) / bimage.getHeight();
            bimage = resize(bimage, width, MAX_HEIGHT);
            int heigt = (bimage.getHeight() * MAX_WIDTH) / bimage.getWidth();
            bimage = resize(bimage, MAX_WIDTH, heigt);
        }
        saveImage(bimage, copyPath);
    }
     
    /*
    Este m�todo se utiliza para cargar la imagen de disco
    */
    public static BufferedImage loadImage(String pathName) {
        BufferedImage bimage = null;
        try {
            bimage = ImageIO.read(new File(pathName));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bimage;
    }
 
    /*
    Este m�todo se utiliza para almacenar la imagen en disco
    */
    public static void saveImage(BufferedImage bufferedImage, String pathName) {
        try {
            String format = (pathName.endsWith(".png")) ? "png" : "jpg";
            File file =new File(pathName);
            file.getParentFile().mkdirs();
            ImageIO.write(bufferedImage, format, file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
     
    /*
    Este m�todo se utiliza para redimensionar la imagen
    */
    public static BufferedImage resize(BufferedImage bufferedImage, int newW, int newH) {
        int w = bufferedImage.getWidth();
        int h = bufferedImage.getHeight();
        BufferedImage bufim = new BufferedImage(newW, newH, bufferedImage.getType());
        Graphics2D g = bufim.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(bufferedImage, 0, 0, newW, newH, 0, 0, w, h, null);
        g.dispose();
        return bufim;
    }
    
    public static void main(String[] args)  {
    	copyImage(null, null);
    }
	
	
	
}
