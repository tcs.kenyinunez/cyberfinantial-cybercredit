package Base;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.File;
import java.io.IOException;

import org.sikuli.script.FindFailed;
import com.jacob.com.LibraryLoader;
import autoitx4java.AutoItX;

public class AutoIt {
	private static final String WINDOWS = "#";
	private static final String CTRL = "^";
	private static final String ENTER = "{ENTER}";
	private static final String HOME = "{HOME}";
	private static final String TAB = "{TAB}";
	private static final String KEY_UP = "{UP}";
	private static final String KEY_DOWN = "{DOWN}";
	private static final String KEY_LEFT = "{LEFT}";
	private static final String KEY_RIGHT = "{RIGHT}";
	private static final String SHIFT = "+";

	private AutoItX autoIt;

	public AutoIt() {
		String JACOB_DLL_TO_USE = System.getProperty("sun.arch.data.model").contains("32") ? "jacob-1.19-x86.dll"
				: "jacob-1.19-x64.dll";
		File file = new File(System.getProperty("user.dir") + "\\libs", JACOB_DLL_TO_USE);
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
		autoIt = new AutoItX();
	}

	/**
	 * Retorna un objeto AutoItX que permite usar los m�todos de la librer�a
	 * AutoItXJava
	 * 
	 * @return
	 */
	public AutoItX getAutoIt() {
		return autoIt;
	}

	/**
	 * Emula el teclado pulsando tecla Windows y Fecha Arriba
	 */
	public void maximize() {
		autoIt.send("#{up}", false);
	}

	// AMPLIAR COLUMNA
	public void ampliarColumna() throws FindFailed, InterruptedException {
		autoIt.mouseClickDrag("left", 195, 439, 282, 439);
	}
	
	// PASAR DE LAPTOP A MONITOR
	public void PasarLaptopaMonitor() throws FindFailed, InterruptedException {
		Thread.sleep(2000);
		autoIt.send("#{rigth}",false);
		autoIt.send("#{rigth}",false);
		autoIt.send("#{up}",false);
	}
	public void Maximizar() throws FindFailed, InterruptedException {
		Thread.sleep(1000);
		autoIt.send("#{up}", false);
	}
	public void Enter()throws FindFailed, InterruptedException {
		//Thread.sleep(2000);
		autoIt.send("{ENTER}",false);
	}
	public String extraerPortapeles() {
		Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable dato = cb.getContents(this);
		DataFlavor dataFlavorStringJava;
		try {
			dataFlavorStringJava = new DataFlavor("application/x-java-serialized-object; class=java.lang.String");
			// Y si el dato se puede conseguir como String java, lo sacamos por pantalla
			if (dato.isDataFlavorSupported(dataFlavorStringJava)) {
			   String texto = (String) dato.getTransferData(dataFlavorStringJava);
			   return texto;
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedFlavorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
}
