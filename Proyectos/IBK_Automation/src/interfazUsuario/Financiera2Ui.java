package interfazUsuario;

import Base.Sikulix;

public class Financiera2Ui {
	public static Sikulix lblFinancieraTwoTitle = new Sikulix("\\Resources\\financiera2\\lblFinancieraTwo.PNG");
	
	public static Sikulix lblHistoricoCoutas = new Sikulix("\\Resources\\financiera2\\lblHistoricoCoutas.PNG");
	public static Sikulix lblNroTable = new Sikulix("\\Resources\\financiera2\\lblNroTable.PNG");
	public static Sikulix lblImporteTotalTable = new Sikulix("\\Resources\\financiera2\\lblImporteTotalTable.PNG");
	public static Sikulix lblMonedaTable = new Sikulix("\\Resources\\financiera2\\lblMonedaTable.PNG");
	//Tabla Historico de Pagos
	public static Sikulix lblFechaPagoTable = new Sikulix("\\Resources\\financiera2\\lblFechaPagoTable.PNG");
	public static Sikulix lblMtoPagoTable = new Sikulix("\\Resources\\financiera2\\lblMtoPagoTable.PNG");
	public static Sikulix lblTipPagoTable = new Sikulix("\\Resources\\financiera2\\lblTipoPago.PNG");
	public static Sikulix lblOperacionTable = new Sikulix("\\Resources\\financiera2\\lblOperacionTable.PNG");
}
