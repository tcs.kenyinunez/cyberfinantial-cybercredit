package interfazUsuario;

import Base.Sikulix;

public class AgregarTelefonoDireccionUi {
	public static Sikulix lblTel = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\lblDiryTel.PNG");
	public static Sikulix btnAgregar = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\btnAgregar.PNG");
	public static Sikulix cmbTipo = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbTipo.PNG");
	
	public static Sikulix cmbIndexCelular = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbIndexCelular.PNG");
	public static Sikulix cmbIndexFamiliar = new Sikulix("\\IBK_Automation\\Resources\\agregarTelefonoyDireccion\\cmbIndexFamiliar.PNG");
	public static Sikulix cmbIndexOficina = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbIndexOficina.PNG");
	public static Sikulix cmbIndexOtra = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbIndexOtra.PNG");
	
	public static Sikulix txtCodArea = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtCodigoArea.PNG");
	public static Sikulix txtTelefonoAgregarTelefono = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtTelefono.PNG");
	public static Sikulix txtExtensionAgregarTelefono = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtExt.PNG");
	public static Sikulix btnAceptarAgregarTelefono = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\btnAceptarAgregarTelefono.PNG");
	public static Sikulix btnOk = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\btnOk.PNG");
	public static Sikulix cabeceraTelefono = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\lblTelefono.PNG");
	
	public static Sikulix lblDireccion = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\lblDireccion.PNG");
	public static Sikulix txtDireccion = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtDireccion.PNG");
	public static Sikulix txtDepartamentoCorto = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtDepartamentoCorto.PNG");
	public static Sikulix txtProvincia = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtProvincia.PNG");
	public static Sikulix txtDistrito = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtDistrito.PNG");
	public static Sikulix txtCP = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\txtCP.PNG");
	public static Sikulix btnLocalizar = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\btnLocalizar.PNG");
	public static Sikulix cmbTipoDireccion = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbTipoDireccion.PNG");
	public static Sikulix cmbIndexCobranzas = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbIndexCobranzas.PNG");
	public static Sikulix cmbIndexCorreo = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\cmbIndexCorreo.PNG");
	public static Sikulix popUpExito = new Sikulix("\\Resources\\agregarTelefonoyDireccion\\popUpExitoDireccion.PNG");
	
}
