package interfazUsuario;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import Base.Selenium;
import Utilitario.Conecci�n.InitSelenium;

public class PagoCuotaHPCUi {
	private static WebDriver driver;
	private Selenium accion;
	public PagoCuotaHPCUi() {
		this.driver = InitSelenium.getDriver();
		accion = new Selenium();
	}
	public WebElement getTxtPago() {
		return driver.findElement(By.id("txtPago"));
	}
	public WebElement getListCuentaContable() {
		return driver.findElement(By.id("ucListaGenericaCuentaContable_ddlLista"));
	}
	public WebElement getListMotivo() {
		return driver.findElement(By.id("ucListaGenericaMotivo_ddlLista"));
	}
	public WebElement getBtnGuardar() {
		return driver.findElement(By.id("fpAnimswapImgFP3"));
	}
	public WebElement getChkSeleccion() {
		return driver.findElement(By.id("dgLista_ctl02_chkSeleccion"));
	}
	public WebElement gettxtCodigo() {
		return driver.findElement(By.id("ucBusquedaCredito_txtCodigo"));
	}
}
