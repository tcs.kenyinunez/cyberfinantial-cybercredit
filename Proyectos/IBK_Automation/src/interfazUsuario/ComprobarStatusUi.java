package interfazUsuario;

import Base.Sikulix;

public class ComprobarStatusUi {
	public static Sikulix lblDetalleCliente = new Sikulix("\\Resources\\lblDetalleClienteTitle.PNG");
	
	public static Sikulix btnBusquedaporNumCuenta = new Sikulix("\\Resources\\botonBusquedaNumeroCuenta.png");
	public static Sikulix txtBusquedaporNumCuenta = new Sikulix("\\Resources\\IngresoBusquedaNumeroCuenta.png");
	public static Sikulix btnBuscar = new Sikulix("\\Resources\\Buscar.png");
	
	public static Sikulix lblCuentaTable = new Sikulix("\\Resources\\lblCuentaTable.PNG");
	public static Sikulix lblStatusTable = new Sikulix("\\Resources\\lblStatusTable.PNG");
	public static Sikulix lblCuentaTableResize = new Sikulix("\\Resources\\lblCuentaTableResize.PNG");
	public static Sikulix lblSaldoVencidoTable = new Sikulix("\\Resources\\lblSaldoVencidoTable.PNG");
	public static Sikulix lblMonedaTable = new Sikulix("\\Resources\\lblMonedaTable.PNG");
	
	//Referencia para capturar Status
	public static Sikulix lblAgencia = new Sikulix("\\Resources\\lblAgencia.PNG");
	
	public static Sikulix lblStatus = new Sikulix("\\Resources\\lblStatus.PNG");
	public static Sikulix btnOkError = new Sikulix("\\Resources\\btnOkError.PNG");
	public static Sikulix txtCA = new Sikulix("\\Resources\\txtCA.PNG");
}
