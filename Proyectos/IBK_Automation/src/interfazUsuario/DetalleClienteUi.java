package interfazUsuario;

import org.sikuli.script.FindFailed;

import Base.Selenium;
import Base.Sikulix;

public class DetalleClienteUi {
	private static Selenium accion;
	public Sikulix lblCuenta;
	public Sikulix getLblCuenta() {
		if(lblCuenta==null) {
			return lblCuenta = new Sikulix("\\Resources\\lblCuentaTable.PNG");
		}
		return lblCuenta;
	}
	
	public Sikulix btnOkError = new Sikulix("\\Resources\\btnOkError.PNG");
	
	public void CliclblCuenta() throws FindFailed, InterruptedException {
		accion.ClickAImagen("\\Resources\\lblCuentaTable.PNG");
	}
	public Sikulix getTxtCA() throws FindFailed, InterruptedException {
		Sikulix txtCA = new Sikulix("\\Resources\\txtCA.PNG");
		return txtCA;
	}
	
	public Sikulix getTxtCR() {
		return new Sikulix("\\Resources\\txtCR.PNG");
	}
	public Sikulix getTxtUsuario() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtUsuario.PNG");
	}
	public Sikulix getTxtTelefono() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtTelefono.PNG");
	}
	public Sikulix getTxtMonto() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtMonto.PNG");
	}
	public Sikulix getBtnGuardar() {
		return new Sikulix("\\Resources\\detalleClientePage\\btnGuardar.PNG");
	}
	public Sikulix getTxtFechaVencimiento() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtFechaVencimiento.PNG");
	}
	public Sikulix getTxtExt() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtExt.PNG");
	}
	public Sikulix getTxtArea() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtArea.PNG");
	}
	public Sikulix getTxtComentario() {
		return new Sikulix("\\Resources\\detalleClientePage\\txtComentario.PNG");
	}
	public Sikulix getLblBitacora() {
		return new Sikulix("\\Resources\\detalleClientePage\\lblGestionBitacora.PNG");
	}
}
