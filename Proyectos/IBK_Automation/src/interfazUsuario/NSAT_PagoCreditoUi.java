package interfazUsuario;

import Base.Sikulix;

public class NSAT_PagoCreditoUi {
	
	public static Sikulix menu = new Sikulix("\\Resources\\NSAT_PagoCredito\\menu.PNG");
	public static Sikulix cmbEntidad = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbEntidad.PNG");
	public static Sikulix cmbIndexInterbank = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbIndexInterbank.PNG");
	public static Sikulix btnAceptar = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnAceptar.PNG");
	//NSAT MENU
	public static Sikulix lblSistemaPago = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblSistemaPago.PNG");
	public static Sikulix lblSATMenu = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblSATMenu.PNG");
	public static Sikulix lblGestionContratosyTarjetas = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblGestionContratosyTarjetas.PNG");
	public static Sikulix lblBusquedaContratos = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblBusquedaContratos.PNG");
	public static Sikulix txtNroContrato = new Sikulix("\\Resources\\NSAT_PagoCredito\\txtNroContrato.PNG");
	public static Sikulix btnBuscar = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnBuscar.PNG");
	
	public static Sikulix lblLoadingInfo = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblLoadingInfo.PNG");
	public static Sikulix btnOpciones = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnOpciones.PNG");
	public static Sikulix lblInclusionPageExternoTitle = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblInclusionPagoExternoTitle.PNG");
	public static Sikulix lblInclusionPageExterno = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblInclusionPagoExterno.PNG");
	
	public static Sikulix txtImporte = new Sikulix("\\Resources\\NSAT_PagoCredito\\txtImporte.PNG");
	public static Sikulix cmbMoneda = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbMoneda.PNG");
	public static Sikulix cmbMonedaIndexSoles = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbMonedaIndexSoles.PNG");
	public static Sikulix cmbMonedaIndexDolares = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbMonedaIndexDolares.PNG");
	
	public static Sikulix txtFecha = new Sikulix("\\Resources\\NSAT_PagoCredito\\txtFecha.PNG");
	
	public static Sikulix cmbTipo = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipo.PNG");
	public static Sikulix cmbTipoIndexCheque = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipoIndexCheque.PNG");
	public static Sikulix cmbTipoIndexGiro = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipoIndexGiro.PNG");
	public static Sikulix cmbTipoIndexInternet = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipoIndexInternet.PNG");
	public static Sikulix cmbTipoIndexMetalico = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipoIndexMetalico.PNG");
	public static Sikulix cmbTipoIndexOtros = new Sikulix("\\Resources\\NSAT_PagoCredito\\cmbTipoIndexOtros.PNG");
	
	public static Sikulix icoLupa = new Sikulix("\\Resources\\NSAT_PagoCredito\\icoLupa.PNG");
	public static Sikulix lblAbonoRegular = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblAbonoRegular.PNG");
	public static Sikulix btnConfirmar = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnConfirmar.PNG");
	public static Sikulix btnOk = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnOk.PNG");
	public static Sikulix lblLoadingInfoSmall = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblLoadingInfoSmall.PNG");
	
	public static Sikulix lblPosicionEconomicaContrato = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblPosicionEconomicaContrato.PNG");
	public static Sikulix lblPosicionEconomicaContratoTitle = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblPosicionEconomicaContratoTitle.PNG");
	public static Sikulix btnMas = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnMas.PNG");
	public static Sikulix btnMasCredito = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnMasCredito.PNG");
	
	public static Sikulix lblAbonosTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblAbonosTable.PNG");
	public static Sikulix lblMonedaTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblMonedaTable.PNG");
	public static Sikulix lblEstadoTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblEstadoTable.PNG");
	
	public static Sikulix lblTotalesTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblTotalesTable.PNG");
	
	public static Sikulix btnPrimerResultMas = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnPrimerResultMas.PNG");
	public static Sikulix btnSegundoResultMas = new Sikulix("\\Resources\\NSAT_PagoCredito\\btnSegundoResultMas.PNG");
	
	public static Sikulix lblFechaOperacionTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblFechaOperacionTable.PNG");
	public static Sikulix lblAnuladaTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblAnuladaTable.PNG");
	public static Sikulix lblResultadoImporteMoneda = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblResultadoImporteMoneda.PNG");
	
	
	//Servira de referencia para capturar los datos de la busqueda
	public static Sikulix lblOficinaTable = new Sikulix("\\Resources\\NSAT_PagoCredito\\lblOficinaTable.PNG");
	
}
