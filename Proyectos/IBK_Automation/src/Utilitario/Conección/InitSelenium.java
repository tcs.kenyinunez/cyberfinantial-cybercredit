package Utilitario.Conecci�n;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import Base.AutoIt;

public class InitSelenium {
    private static WebDriver Driver;
    private static WebDriverWait Wait;
    
    public static WebDriver getDriver(){
        return Driver;
    }
    
    public static WebDriverWait getWebDriverWait(){
        return Wait;
    }
    
    // Iniciamos la ejecucion de las pruebas y la navegacion. 
    
    public void IniciarSelenium(String navegador) throws IOException{   	
    	switch(navegador.toLowerCase()) {

    	case "internetexplorer":  
     		System.setProperty("webdriver.ie.driver","src//Webdriver//IEDriverServer.exe");
 	    	DesiredCapabilities IECapabilities = DesiredCapabilities.internetExplorer();
 	    	IECapabilities.setCapability("requireWindowFocus", true);
 	    	IECapabilities.setCapability("ignoreZoomSetting", true);
 	    	IECapabilities.setCapability("ignoreProtectedModeSettings", true);
 	    	Driver=new InternetExplorerDriver(IECapabilities);
 	    	Driver.get("http://s417va7:9080/bei010Web/");
    		break;
    	case "internetexplorer2":  
     		System.setProperty("webdriver.ie.driver","src//Webdriver//IEDriverServer.exe");
     		DesiredCapabilities IECapabilities2 = DesiredCapabilities.internetExplorer();
 	    	IECapabilities2.setCapability("requireWindowFocus", true);
 	    	IECapabilities2.setCapability("ignoreZoomSetting", true);
 	    	IECapabilities2.setCapability("ignoreProtectedModeSettings", true);
 	    	Driver=new InternetExplorerDriver(IECapabilities2);
 	    	Driver.get("http://intranetib/sda/");
    	}
    	
    	Driver.manage().window().maximize();
        Wait = new WebDriverWait(Driver,300);    	
    }
  
    // Finalizamos la navegacion y la ejecucion.
    public static void FinalizarSelenium() throws InterruptedException{
        Thread.sleep(2000);
        Driver.quit();
        AutoIt auto = new AutoIt();
        auto.getAutoIt().send("{ENTER}",false);
    }
}
