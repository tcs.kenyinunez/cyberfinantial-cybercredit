package Utilitario.Conecci�n;

import java.io.File;

import com.jacob.com.LibraryLoader;

import autoitx4java.AutoItX;

public class AutoIT_Clipboard {
	private AutoItX control;
	public AutoIT_Clipboard() {
		String JACOB_DLL_TO_USE = System.getProperty("sun.arch.data.model").contains("32") ?
				"jacob-1.19-x86.dll" : "jacob-1.19-x64.dll";
		File file = new File(System.getProperty("user.dir")+"\\libs", JACOB_DLL_TO_USE);
		System.setProperty(LibraryLoader.JACOB_DLL_PATH, file.getAbsolutePath());
		control = new AutoItX();
	}
	public AutoItX getAutoIt() {
		return control;
	}
}
