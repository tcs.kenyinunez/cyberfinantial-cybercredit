package Utilitario.Reporte;

import java.util.List;

public class RegistroFotos {
	
	public int numRegistro;
	public String ValorGlobal1;
	public String resultadoEjecucion;
	public String mensaje;
	public List<Imagen> imagenes;
	
	public RegistroFotos(int numRegistro, String codigoRelacion, String resultadoEjecucion, String mensaje,
			List<Imagen> imagenes) {
		super();
		this.numRegistro = numRegistro;
		this.ValorGlobal1 = codigoRelacion;
		this.resultadoEjecucion = resultadoEjecucion;
		this.mensaje = mensaje;
		this.imagenes = imagenes;
	}
	
	public List<Imagen> getImagenes() {
		return imagenes;
	}
	public void setImagenes(List<Imagen> imagenes) {
		this.imagenes = imagenes;
	}
	public RegistroFotos() {
		super();
	}
	
	public int getNumRegistro() {
		return numRegistro;
	}
	public void setNumRegistro(int numRegistro) {
		this.numRegistro = numRegistro;
	}
	public String getCodigoRelacion() {
		return ValorGlobal1;
	}
	public void setCodigoRelacion(String codigoRelacion) {
		this.ValorGlobal1 = codigoRelacion;
	}
	public String getResultadoEjecucion() {
		return resultadoEjecucion;
	}
	public void setResultadoEjecucion(String resultadoEjecucion) {
		this.resultadoEjecucion = resultadoEjecucion;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
}
