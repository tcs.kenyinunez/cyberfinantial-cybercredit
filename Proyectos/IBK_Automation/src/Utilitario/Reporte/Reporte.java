package Utilitario.Reporte;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import javax.imageio.ImageIO;
import java.awt.Image;

public class Reporte {

	// DECLARACI�N DE VARIABLES DEL EXCEL Y REPORTE
	public String columnaCodigoRelacion = "";
	public String columnaEstadoEjecucion = "";
	public String columnaMensajeObtenido = "";
	private String rutaCargaDataInput = "";
	private String rutaDataOutput = "";
	private String rutaReporte = "";
	private String rutaReporteEjecucion = "";
	private String rutaReporteProceso = "";
	private String nombrePrueba = "";
	private String listaOK = "";
	private String listaInconcluso = "";
	private String listaFallidos = "";
	private boolean eliminado = false;
	boolean inconcluso = false;
	public static String nombreSistema = "";
	public static String nombreProceso = "";
	LeerExcel leerExcel;

	////////////////////////////////////
	private Image FOTO_ORIGINAL;
	private BufferedImage screenShot;
	private int clipX = 113;
	private int clipY = 451;
	private int clipAncho = 133;
	private int clipAlto = 20;
	
	private int clipX1 = 691;
	private int clipY1 = 183;
	private int clipAncho1 = 615;
	private int clipAlto1 = 65;
	
	private int clipXI1 = 2434;
	private int clipYI1 = 278;
	private int clipAnchoI1 = 20;
	private int clipAltoI1 = 15;
	
	private int clipXE1 = 1130;
	private int clipYE1 = 671;
	private int clipAnchoE1 = 100;
	private int clipAltoE1 = 20;
	////////////////////////////////////

	public boolean getInconcluso() {
		return inconcluso;
	}

	public void setInconcluso(boolean inconcluso) {
		this.inconcluso = inconcluso;
	}

	public int contadorFallido = 0; // Cuenta los escenarios fallidos e inconclusos
	public int contadorInconcluso = 0; // Cuenta los escenarios que no llegan a la segunda sesi�n
	public int contadorSatisfactorio = 0; // Cuenta los escenarios que terminan ambas sesiones
	public int contadorRegistros = 0; // Cuenta el numero de escenarios ejecutados
	String fechaInicio = "";
	String fechaFin = "";

	public void ResumenEjecucion() {
		System.out.println("---------------------------------------------");
		System.out.println("RESUMEN DE EJECUCIONES");
		System.out.println("---------------------------------------------");
		System.out.println("SATISFACTORIO:" + contadorSatisfactorio);
		System.out.println("INCONCLUSO:" + contadorInconcluso);
		System.out.println("FALLIDOS:" + contadorFallido);
		System.out.println("---------------------------------------------");
	}

	public void setFechaInicio() {
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fechaInicio = DateFormat.format(date);
		this.fechaInicio = fechaInicio;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaFin() {
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		String fechaFin = DateFormat.format(date);
		this.fechaFin = fechaFin;
	}

	public String getFechaFin() {
		return fechaFin;
	}

	public void contadorRegistrosAumentar() {
		contadorRegistros++;
	}

	public Reporte(String columnaEstadoEjecucion, String columnaMensajeObtenido, String columnaCodigoRelacion,
			String rutaCargaDataInput, String nombreSistema, String nombreProceso) {
		this.columnaEstadoEjecucion = columnaEstadoEjecucion;
		this.columnaMensajeObtenido = columnaMensajeObtenido;
		this.columnaCodigoRelacion = columnaCodigoRelacion;
		this.rutaCargaDataInput = rutaCargaDataInput;
		this.nombreSistema = nombreSistema;
		this.nombreProceso = nombreProceso;
		this.rutaReporte = "C:\\Automatizacion\\Reportes\\";
		leerExcel = new LeerExcel();
	}

	public void VerificacionSatisfactoria(int posicionFila) {
		contadorSatisfactorio++;
		if (contadorSatisfactorio == 1)
			listaOK += nombrePrueba;
		else
			listaOK += ", " + nombrePrueba;
		System.out.println("VERIFICACION SATISFACTORIA");
		leerExcel.ActualizarExcel(posicionFila, columnaEstadoEjecucion, "Satisfactorio", rutaDataOutput);
	}

	public void VerificacionFallida(int posicionFila, String mensajeError) throws Exception {
		if (!mensajeError.equals("Inconcluso")) {
			mensajeError = "Escenario fallido";
			contadorFallido++;

			if (contadorFallido == 1)
				listaFallidos += nombrePrueba;
			else
				listaFallidos += ", " + nombrePrueba;

			System.out.println("VERIFICACION FALLIDA");
			leerExcel.ActualizarExcel(posicionFila, columnaEstadoEjecucion, "Fallido", rutaDataOutput);
			leerExcel.ActualizarExcel(posicionFila, columnaMensajeObtenido, mensajeError, rutaDataOutput);
			CapturarEvidencia("Error encontrado");
		}
	}

	public void VerificacionInconclusa(int posicionFila, String mensajeError) throws Exception {
		contadorInconcluso++;
		inconcluso = true;

		if (contadorInconcluso == 1)
			listaInconcluso += nombrePrueba;
		else
			listaInconcluso += ", " + nombrePrueba;

		System.out.println("VERIFICACION INCONCLUSA");
		leerExcel.ActualizarExcel(posicionFila, columnaEstadoEjecucion, "Inconcluso", rutaDataOutput);
		leerExcel.ActualizarExcel(posicionFila, columnaMensajeObtenido, mensajeError, rutaDataOutput);
		CapturarEvidencia("Escenario inconcluso encontrado");
		throw new Exception("Inconcluso");
	}

	public String nombreCompletoImagen;

	public String CapturarEvidencia(String nombreImagen) throws AWTException, IOException {
		nombreImagen = nombreImagen.replace(" ", "_");
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd_MM_yyyy__HH_mm_ss");
		String fechaCaptura = DateFormat.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyhhmmss");

		Robot robot = new Robot();
		BufferedImage screenShot = robot
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		nombreCompletoImagen = fechaCaptura + "__" + nombreImagen + ".jpg";
		System.out.println(rutaReporteEjecucion);
		ImageIO.write(screenShot, "JPG", new File(rutaReporteEjecucion + "\\" + nombreCompletoImagen));
		System.out.println("-->Imagen Capturada: " + rutaReporteEjecucion + "-" + nombreCompletoImagen);

		String retorno;
		retorno = rutaReporteEjecucion + "\\" + nombreCompletoImagen;
		return retorno;
	}

	public String nombreCompletoImagenCuenta;

	public String CapturarEvidenciaCuenta(String nombreImagen) throws AWTException, IOException, InterruptedException {

		nombreImagen = nombreImagen.replace(" ", "_");
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd_MM_yyyy");
		String fechaCaptura = DateFormat.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		Robot robot = new Robot();
		BufferedImage screenShot = robot
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		screenShot = screenShot.getSubimage((int) clipX, (int) clipY, (int) clipAncho, (int) clipAlto);

		nombreCompletoImagenCuenta = fechaCaptura + "__" + nombreImagen + ".jpg";
		ImageIO.write(screenShot, "JPG", new File(rutaReporteEjecucion + "\\" + nombreCompletoImagenCuenta));
		System.out.println("-->Imagen Capturada: " + rutaReporteEjecucion + "-" + nombreCompletoImagenCuenta);

		String retorno;
		retorno = rutaReporteEjecucion + "\\" + nombreCompletoImagenCuenta;
		return retorno;
	}
	
	public String numCuotas;

	public String CapturarEvidenciaCantCuotas(String nombreImagen) throws AWTException, IOException, InterruptedException {

		nombreImagen = nombreImagen.replace(" ", "_");
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd_MM_yyyy");
		String fechaCaptura = DateFormat.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		Robot robot = new Robot();
		BufferedImage screenShot = robot
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		screenShot = screenShot.getSubimage((int) clipX1, (int) clipY1, (int) clipAncho1, (int) clipAlto1);

		numCuotas = fechaCaptura + "__" + nombreImagen + ".jpg";
		ImageIO.write(screenShot, "JPG", new File(rutaReporteEjecucion + "\\" + numCuotas));
		System.out.println("-->Imagen Capturada: " + rutaReporteEjecucion + "-" + numCuotas);

		String retorno;
		retorno = rutaReporteEjecucion + "\\" + numCuotas;
		return retorno;
	}
	
	public String importe;
	
	public String CapturarEvidenciaPago(String nombreImagen) throws AWTException, IOException, InterruptedException {

		nombreImagen = nombreImagen.replace(" ", "_");
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd_MM_yyyy");
		String fechaCaptura = DateFormat.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		Robot robot = new Robot();
		BufferedImage screenShot = robot
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		screenShot = screenShot.getSubimage((int) clipXI1, (int) clipYI1, (int) clipAnchoI1, (int) clipAltoI1);

		importe = fechaCaptura + "__" + nombreImagen + ".jpg";
		ImageIO.write(screenShot, "JPG", new File(rutaReporteEjecucion + "\\" + importe));
		System.out.println("-->Imagen Capturada: " + rutaReporteEjecucion + "-" + importe);

		String retorno;
		retorno = rutaReporteEjecucion + "\\" + importe;
		return retorno;
	}
	public String estado;
	
	public String CapturarEvidenciaEstado(String nombreImagen) throws AWTException, IOException, InterruptedException {

		nombreImagen = nombreImagen.replace(" ", "_");
		Date date = new Date();
		SimpleDateFormat DateFormat = new SimpleDateFormat("dd_MM_yyyy");
		String fechaCaptura = DateFormat.format(date);

		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyy");

		Robot robot = new Robot();
		BufferedImage screenShot = robot
				.createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));

		screenShot = screenShot.getSubimage((int) clipXE1, (int) clipYE1, (int) clipAnchoE1, (int) clipAltoE1);

		estado = fechaCaptura + "__" + nombreImagen + ".jpg";
		ImageIO.write(screenShot, "JPG", new File(rutaReporteEjecucion + "\\" + estado));
		System.out.println("-->Imagen Capturada: " + rutaReporteEjecucion + "-" + estado);

		String retorno;
		retorno = rutaReporteEjecucion + "\\" + estado;
		return retorno;
	}
	
	public void GuardarDatoEnExcel(int posicionFila, String columna, String texto) {
		leerExcel.ActualizarExcel(posicionFila, columna, texto, rutaDataOutput);
	}

	public void GuardarCodigoRelacion(int posicionFila, String texto) {
		leerExcel.ActualizarExcel(posicionFila, columnaCodigoRelacion, texto, rutaDataOutput);
	}

	public void LimpiarDatosPagoNoUbicadoSaldoMinimo(int posicionFila) {
		leerExcel.ActualizarExcel(posicionFila, columnaCodigoRelacion, "", rutaDataOutput);
		leerExcel.ActualizarExcel(posicionFila, columnaEstadoEjecucion, "", rutaDataOutput);
		leerExcel.ActualizarExcel(posicionFila, columnaMensajeObtenido, "", rutaDataOutput);
	}

	public String IniciarReporte(String correlativo) throws FileNotFoundException, IOException {
		nombrePrueba = correlativo;
		String rutaReporteSistema = rutaReporte + "\\" + nombreSistema;
		rutaReporteProceso = rutaReporteSistema + "\\" + nombreProceso;
		String rutaReporteCorrelativo = rutaReporteProceso + "\\" + correlativo;
		CrearCarpeta(rutaReporteSistema);
		CrearCarpeta(rutaReporteProceso);
//	ElminimarArchivos(rutaReporteCorrelativo);
		rutaReporteEjecucion = rutaReporteCorrelativo;

		if (!eliminado) {
			eliminado = true;
			EliminarArchivosCarpeta(rutaReporteProceso);
			String excelOutput = rutaReporteProceso + "/Output.xls";
			rutaDataOutput = excelOutput;
			CopiarArchivo(rutaCargaDataInput, rutaDataOutput);
			leerExcel.ActualizarExcel(4, "J", "CodigoRelacion", rutaDataOutput);
			leerExcel.ActualizarExcel(4, "K", "ResultadoEjecucion", rutaDataOutput);
			leerExcel.ActualizarExcel(4, "L", "Mensaje Obtenido", rutaDataOutput);
		}

		CrearCarpeta(rutaReporteCorrelativo);
		return rutaReporteCorrelativo;

	}

	private void EliminarArchivosCarpeta(String rutaCarpeta) throws IOException {
		File carpeta = new File(rutaCarpeta);
		File[] archivos = carpeta.listFiles();

		for (File archivo : archivos) {
			if (archivo.isDirectory()) {

				System.out.println("Eliminando carpeta:" + archivo.getAbsolutePath().toString());
				ElminimarArchivos(archivo.getAbsolutePath().toString());
				archivo.delete();

			}
		}
		ElminimarArchivos(carpeta.getAbsolutePath().toString());
	}

	private void CopiarArchivo(String origen, String destino) throws FileNotFoundException, IOException {
		File copied = new File(destino);
		try (InputStream in = new BufferedInputStream(new FileInputStream(origen));
				OutputStream out = new BufferedOutputStream(new FileOutputStream(destino))) {

			byte[] buffer = new byte[1024];
			int lengthRead;
			while ((lengthRead = in.read(buffer)) > 0) {
				out.write(buffer, 0, lengthRead);
				out.flush();
			}
		}
	}

	private void CrearCarpeta(String rutaCarpeta) {
		File crea_carpeta = new File(rutaCarpeta);

		if (!crea_carpeta.isDirectory())
			crea_carpeta.mkdirs();
	}

	public void ElminimarArchivos(String rutaCarpeta) {
		File dir = new File(rutaCarpeta);

		for (File file : dir.listFiles()) {
			if (file.isDirectory())
				file.delete();
			file.delete();
		}

	}

	// DISE�O DEL REPORTE
	public static void crearArchivo_html(float pruebasinconclusas, String ruta, String fechahoraInicio,
			String fechahoraFin, float pruebasejecutadas, float pruebasok, float pruebasfalladas, float porcentajeOK,
			float porcentajefalladas, float porcentajeinconclusas, List<RegistroFotos> Registros) {
		String carpetas = ruta;
		File crea_carpeta = new File(carpetas);

		if (crea_carpeta.isDirectory()) {

		} else {
			crea_carpeta.mkdirs();

		}

		FileWriter flwriter = null;
		try {

			flwriter = new FileWriter(carpetas + "\\Reporte Resumen.html");

			BufferedWriter bfwriter = new BufferedWriter(flwriter);

			bfwriter.write(" <!DOCTYPE html> " + "\r\n");
			bfwriter.write(" <html>" + "\r\n");
			// LOGOTIPO DEL REPORTE
			bfwriter.write(
					"<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAclBMVEX///89hsYwfsJAiMcvfcL7/f4zgMM3gsT2+fw6hMXq8vlFi8hMj8pBiccre8EmeL93qtddms9/r9mJtdyQud7U5PLk7veqyubv9frZ5/SdwuKz0Om91uzF2+5Sk8xtpNRwptWWvuBZl87A2OzM3/Adcr0OfrtnAAAKS0lEQVR4nO1a25ajOg41xtiYW7gTDIGQUP//iyPZkKQuvU5Pp6dSZ432Q2LABm3LkiUBYwQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgOITvENxPYSt4f/V+KnilyF8jT5M70pIFbLVN30Cz8x8uJlO094/nV4v9GTPXd/AcpK8yT2tPXmDWO+E9XPWBSK8VNuX6ark/4axS3/N83/6onLEowSM4boCI9B3sqRiILNwe8/LVcn9CkUmpPISWKoMlM0h75POaMXNQUu4XuW7ZcVK+PUZSPwunYRjPIKyvynEYYMWUHJUDx9OR9cMwGF97vp6gNYRsRFp4nXfsBxp8LUBwPth2C3KDYdxPREhEFa7njERQgernmXsA5o1KwDmGtgEjUMUMwsrZXuwdkRAv9rjI0jLVnvbbH6gSR2RAyYKL8n2xnDiK3MPxTgSvIUtPxy0Yis/NTyYSsKs1lzVINMhavScCngFMXZ7RjMCEwleL/RkPGsk3Ge2/lf5GBLZK5VnTGSX0l9efp5I7kSjeHG/HrWYeNRKwEgjoJGIhOmGZv1rsz7gTwU3Eh7neGJXvltb9XG1ddP/jVIJEfIk7wwxbuU6OzK0xjdteD9c8jUQ63NXViGtM+z/S3KvMuV/WoBEIa+Mj+i1hrI24yCsopKedjYeJgnPFq+X+hFrHcayHreHbgDC8+NAsQtbHKTYC1sB/rMB+MLBU2LF5sdyfcIwQMNX2P3o8CYqw/7Dawr0Xe98mEAgvR3D7cX+728dAdm996Pjhyn79jv3w/Q0eGreb3ToFT243wS/afxv/+10xGqo8L01v2115zpfGPvZolsX52LHurCgjdKwGONfUS2uvnGoT4XUTwoh2MQbGGAT44Wgxy7aFXJfFlSH6/ZasMSXcbAy7ZcEYJmCnpX6iVAF3WBJ+ALwtcGgSccgOmVceUS4pDmfbK3+bgFhbSOx4GDBJz0b79OpNgqzng47g4PoGVwXP4DfrsRPfxrPyICCMgbUzHLiVdp097HbQ4XA42CzymBy89s+JgBBCprnp6mLFXFZOy2AmJTBmamM/5Sgvq/A4KITOO5Nj2aqT/slOQ81jELmUCRJpzuf8nHpTDv8g06wTfys+VDLGEhJENCptcPY1l0UNNyuxPmPDSiPV8Mzyq4Qu7KKCp6xSX3BSjoUHkR6k5V7sAqUyAyInpSo7dViR497VaqQWIBhkULdqSZiIrQR0TLwuEU64ise+TUxGieWjNlba7F7iqiTEM+HEn0rvVwVp9u4wZumt1on0NrRtUw+WGsaulkgnIaINnVf7NZFjwm0CAqtIJNEkXDZSHcpcXIKdSC51d3dXOY9byI69p0Kyitt426L3pZuUAJ80wrzxsRRpu2kEstuitVd/hwjIB2s/Fy7TBSKDhATAEWlTrLbcllHviZpN2VO1PAi5k00EmEK1pQ44+3wBImJsNeTejkhQcJ1YN/RbRCJUJmjRZrpVVoSFhIm3REYNd3/YT2pxGTH/egJhIovbTmj4Zm4BPqpCjXRgx3J0RFhUcCn1uf0dIjAtHHQZbRYDRMDGIOsdFQT1HVfv6nZRqlPxXPr1D0REB3oHe68sERZ0hVY8adnvaCTPzvBb4toK3A1ypZpVAhHzngjcBbzMc7F+cJHx10tLGkeELVKezMHVq1iwzprPqBHtiiO1wCXxBZEo1tM8z5OW406k9Xm+Klha8KD63fw3vlieUgjuAHLYo56bsYNvl/q0EQku2bnLHBH8KRQsmUFvwyrnDJBI8EAEV5avpJTKl+VOBFjHY6wb1nj68k5sIFI/RcS6w+lWMC8UiI9OEZ8UOiLordJ5K7yhmy7RWTYeLD30nrPEEsQnjQTIcGzWZp0UdnBEjpMqEiACD1LmrxIBnLmebNBwDNlJ6gR9R3vRsKPvRMDPe0gkGvFRTYIFhnDSMY4yaEvBphEb7G5LC/Y3V3Oo0fAckcAV6RvcvTxdYQ0mOP41IseLUF5RVnlywoUi09JUibS+po1t6Yc12scyzyCKss5jaTfrTqi0MmeP29ikVGlh0TgiNgZZXCiosY5nYxzcn1TqNdbrKZnkVVWcNyLyWRsBq6xiFwtigGtsAMknq/hWH4xzTRn6HqMEXJOJDYSZie1RYatv54MUiOzEIt/GgPlBuY06SjNYW6WNOmHvU9y9jRsLjeMPiXVWqzxUfyHMj4a6qsxotXyEdj04hYdD12+NAZdV392vAc2uqpare/raDQ4RDjphwN+NW7/rAEPW/fDU7TdYTVXVXRM4Cbq/XDIKPvz/us9PKx5aoJHe8s2v0tI9dX1Ifj/2vCW4tyyXPd7ly1Q3+PgAwv8PdpU/lEbYl+WOj4bzUBp5GPlvW0E/T9oG/K7NpfoNGIgc275vrQ+N3ImtA4uafgthgwbfxUOfdh/ovOzVnf1urJC6C6VL3OPdVw4QSLA6TbXWyQy7xervr9FXjA1r7jdWGx0O5GpqISZN7fcQGFmyJZYZBETf/gILAkN9KSbYc/FjAD9N0xgCCche/eQySY7hbyFTl/zWGNZDSI9EjjPQny4X/4JBmIcD/RhDZCHj4uKn366SWfqQSoTXFpMLnkctwEW6LZxNlN8yI100Fk4SomWXZEFGrOY1DILWZVyNHRiykev8CGdP380jspWoW3srDQR7yG6wPAHB3hnPrR5Gho4IpH/3us5DfF/xF30EcYzVJdpc7Z2I0wjDz2aw/DJLm1FZ63BEIOe4JVlb8uKasPq65wvVf4ISTGQLCpFIuL1EA9nc21yca4OmsicflsjVZSh3Iv3RvZFbJeQg7ff7ZkixE670pcM4Pkq1H+NrzdESOTWns7OO3n5a1ng2L7JEus1s3D1gPeE4jbnWAjlIWr7ixWhUx1xixgFErPv1cPrBEXE4nTijnTlkG4brdSdyK8XsRDSMc9XQUwHZiFpewIRFZlJ8wuyd530DsF4rPeeT9racC13uLAp21wjv3i2tEw7cDOWUa088Var+IwS2KKcVFk/F3dgF2Eg4S5fpQSbbHW11LdhtROYPRMTDt4B2r9TyBZ+iBUHIBqkH1Ei5FRc2j9rbj5tssagcuN/fNHJMsLzgEhzUSNrf3sDZv4J/+3dP7nlGOY1U+2lHJLhwV06osyLHbzVvVcdFeNPthY1Vn8NW9pq+/YuOYK6G02lJsMrdxqrojOlMt5d+0Ixt0ITfzVg/hTtFipqZuU7q4XQ1He6C6WIHgvEU5rqOs+bdNxNpxEEomQl8NdV6Uh6yLDvEIcszP7KVRFd2BtWoFO0AuB1sqSc8S56Bq8XOZaYzGCjeDJvfhFRCiG//EvhoiimZLiXGFVGxIYd5nwqUO5ont9oM9GFby737YmN+gaFFFbJlcuOmgV1LvN/83fpABPev9INP1YXP/4+t2wf+7167b9//fzf+vOzzz8UkAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIBAIBAKBQCAQCAQCgUAgEAgEAoFAIPx3+A/AY62iB0abxQAAAABJRU5ErkJggg==\" style=\"position: absolute; top: 0; left: 0;\" />");
			bfwriter.write(
					"<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxASEBUSEhIRFRUXDRUVFxUVFRcVFhUXFRcWFxcVFRUYHSggGBolHRUXITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGy0lHSUtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAMgAyAMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAAAQQFBgcDAv/EADkQAAIBAwAIAgkDAwQDAAAAAAABAgMEEQUGEhchU5HSMVETFCI0QWFxdLIyctEHgaFiscHwFSNS/8QAGgEBAAIDAQAAAAAAAAAAAAAAAAEDAgQFBv/EACsRAQACAQMDAwQCAwEBAAAAAAABAgMEERMVIVESMVIFIjIzQXEUYYFCI//aAAwDAQACEQMRAD8A7eQgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAIAkAAAAQBIDIBAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADIBAAAAAAAAAAAAAAAAAAAAAAAAAAgAHONI6/XNOtUgqdHEasorKlnEZNcePyK5vtLhZvqt6ZJrER2lXf9Rbrl0eku4x5VXWMnxg3i3XLo9Jdw5TrGX4wbxbrl0eku4cv+jrGX4wbxbrl0eku4cs+DrGX4wbxbrl0eku4cp1jJ8YN4t1y6PSXcOU6xl+MG8W65dHpLuHLJ1jL8YN4t1y6PSX8jlOsZfjBvFuuXR6S7hynWMnxg3i3XLo9JfyOU6xk+MG8W65dHpLuHLJ1jJ8YN4t1y6PSXcOU6xl+MG8W65dHpLuHLPg6xk+MG8W65dHpLuHKdYy/GDeLdcuj0l3DlOsZfjBvFuuXR6S/kcp1jJ8YN4t1y6PSXcOU6xl+MG8W65dHpLuHLJ1jL8YN4t1y6PSXcOWfB1jL8YN4t1y6PSXcOWTrGX4wbxbrl0eku4cs+DrGX4wbxbrl0eku4cpH1jJ8YdD0PdSq0KdSSSc6UZNLwWVnhkud3Deb0iy4wtcL057zW+5qfkzWt7vGan91v7YqpdxTwbePQ5LxvCzHo8l43h8+vw+fQs6bk8rOn5D16PzHTcp0/I+qd5FvHEwy6DJjr6pYZNFkpXeVg0moEIgYABkAAAAAGAAABgAAAAMAGEw7dqx7nQ+3h+KNqPZ7HS/qqyZLYcL057zW+5qfkzXn8njNT2zW/uWsVP1P9zPT4vtxxLvYvtxxL3jZSfkauT6jjr2hr5PqGOvaHo7HCbb+BTX6jN7bRCmPqE2ttCtbfrX1N3Uz/8AGZlt6j9M7tu1VoRqXdKE0pRc+KfgzzdPycjRUi+aIn2WNc7WFO8lCnFRWI4UV5+SF47s9djiub01hFvqlezjtKk8Y+LSfQccor9PzTXfZjJWc6dZU6sHF7cU0/LJG3faVMYrVyRW8Ny1r1Rk5U/VaKS2HtYfx4Y8WW2pv7Opq9BvtOOGnT0bWVV0dhuonhxXH5/D6lXplyeG829MR3e15oS5pR2qlKcY+eOC+omkwzvpctI3mFS0talWahTi5SfwRERuqpjtedqw2nVnV9wvVSuqaadvKSi3n4pZ4Fla7T3dLSaT05YjJDHaS0DXncVvQUZuEa00tlcEk8YRFqTv2U59JknJPor2Ya4t505OM4uLXwawzCYmGnfHak7Whk7PVm8qRU4UnstZTeFlfLJlFJlsU0WW8bxDH3tlVoy2KsHGXk1/lGMxMKMmK2OdrQu6M1cuq8dqnTbj5vgn9MmUUmV2PR5ckb1hX0nomvbvFWDjnwfwf0ZE1mGGbT3xT90Nlu9H0loeFVQiqja9rHH9bLNvtdK+Kv8AiRbbu1a0sK1VN06cpKPi0uC+pVETLl0w3vG9YV5LAV/y7bqx7nQ+3h+KNmPZ7HS/qqyZLYcL0571W+5qfkzWt+Txmp/db+2sz/W/3Hp6/p/47ld+H/jMLwPM3j7peet+Uvmt+l/tZng/ZDLD+cMVbfrX7j0Oq/TLvar9LctTffaX7zzlPycrQ/vjZtNS2jU03iSylDax80uBnt9zpTji2t2ljdZtabuF3UhTnsxhPZUccHjzMbXmJUarXZaZprHtDI65pVKVpcNJTlOCf91n/v1MrfxK/WfdGPJt37I/qLpGtSqUlTqSgnTbaTxnihkmYY/Us18cxFZ/h96sWlVWM69JKVxUk/ak+KWcePVk132ZaTHbgm9fyla1ft76Dn67KLouDztNPH0Jrv8AytwVyxE809lPU2NOnSu69NJuMp7Lf/zGOUiKdt1ekrXHW96qWpWkqtxpBTqy2n6CSXyWU8IxpaZlToc9suo3srazaz3Ku5xpTcIwqOKikuLXBt/V5IteYlVqtbkjNMV7RDI61JXOj6N04pVMpNpeOXstfTKyTbvXdfq45tPXLt3VLfR+lXCMvS+jSitlSmo8F4cBEWVUxaj0xO+zI65WznQtHUcXN1Iwk48c7WE8P4mVv4bWspvSk29068aYrWvoqNCWxH0WW0v7JfTgRe23sx12otg9NMfgp3UrzRFWdbDlByxLHFuKUk/8kxO9e6a3nUaS1r+8Pu2sHX0RRpJpbU48X+9iI3qypinJpYqxGtN8raHqNBOKSXpJeDm38zC8+ntDU1eXgrxUaeytyY93btWPc6H28PxRtR7PY6X9VWTDYcL0571W+4qfkzXt+Txmp/db+2szftP93/J6fHG+KIjw72ON8URHhed9FebOPH0/JaZ3cmNDktbu8qt/lYSNnF9O9Nt5ls4vp/pneVe1/Wvqbmq7YZht6ntimG36pVYxvKUpNJKfFt4XU83SfucjQ2iueJltVK+pf+ZdT0kNj0L9raWz4efgWf8Ap0a5Kf5nq37NS1nqxleVpRaadVtNPKf0ZXf3czWWi2e0x5bJrTfU3Y2qhODlFwbSkm1iPxRZae0Ojqc1YwU2n2ZHTdta6RhTqxuIU3GGGm1lZxwab4NYJmIsuzVxamsTupaGv7eEatjOtiKl/wCusnhZaWeP1IiYjsqwZaVicM2/6r6T0DGFOU536nFRbUU+L8l4vJE1/wBq8+m2rMzk3Tqjd046PuoynCMmp4Tkk37GOCfiKT9qdJkpGntEz3a7q3pP1a5hVfGKbUl/plwf8/2MKztLQ02fiy+ptek9X7S6qu4p3VOEZ+1JNrOfi/FYLJrEzu6eXS4c9uStvdW1l05QhCja0Htwpzi5P4PZecfPzItaI7Qw1OqpWK46e0LmmrKnfyjVp3cIx2EnCTxs+fDJMx6v5W5qV1O1ottCnrFpChH1W2pzU1SqR2p54cGviLTHZVqc1I9GOJ32eH9RrqnUrU3CUZJUMZi08PL8jHIq+qXre9ZrP8LOr93Tjom4g5wUnKpiLkk3mEfBE1/BZpslY0dqz7lxfQWhqcY1IqopReypLaWJt+HiTv8Aaz54jSxtPd5K7o39vs1ZQp3FOPszk0lNeTbIiYtHdTGSmpxem/a0NPqQw2njg8cHnoyrbZy5jadnbNWPc6H28PxRtR7PYaX9VWTDYcL057zW+4qfkzWt+Txmp/db+5YiVlFvPE3q/UMlY2hdXW5KxtB6jH5k9SyMv8/KeoR+Y6llR/n5X3TtIxef9yrLrL5I2lhk1d8kbS92zTansBO8gQBO4SbyEI3TklMzKCEbgBMlO8wEIEwneYCUBAEp3kIRuEgEx7u3ase50Pt4fijZj2ex0v6qsmS2HONI6hXNStUnGpRSlVlJZcs4k2+Ps/MrnHvO7g5fpV75JtEq27u65lHrLtI4lfSL+Td1dcyj1l2jiOkZPJu6uuZR6y7RxHSMnlO7u65lHrLtHEdIv5N3d1zKPWXaOI6Rfyjd3dcyj1l2jiOkZPJu6uuZR6y7RxHSL+U7urrmUesu0cR0i/lG7q65lHrLtHEdIv5N3V1zKPWXaOI6RfyburrmUesu0cR0jJ5N3V1zKPWXaOI6RfyburrmUesu0cR0jJ5Tu6uuZR6y7RxHSL+Ubu7rmUesu0cR0jJ5N3V1zKPWXaOI6Rk8m7q65lHrLtHEdIyeTd1dcyj1l2jiOkX8m7q65lHrLtHEdIv5N3V1zKPWXaOI6Rk8m7q65lHrLtHEdIyeTd1dcyj1l2jiI+kX8uiaItXSoU6Umm4UoxbXhlJItd7DT0Uiq4FgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGAAAACAAAAAAAAAAAAAAAAQBIAAAAAQBIAAAAAAAACAJAAMgAAAAAyAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABgMAAAAAAAAAAAAAA/9k=\" style=\"position: absolute; top: 0; right: 0;\" />");
			bfwriter.write(" <style type='text/css'>" + "\r\n");
			bfwriter.write(" .oculto {display:none}" + "\r\n");
			bfwriter.write(" </style>" + "\r\n");
			bfwriter.write(" <head>" + "\r\n");
			bfwriter.write(" <meta http-equiv=\"Content-Type' content='text/html; charset=utf-8\" />" + "\r\n");
			bfwriter.write(" <meta name=\"viewport\"content=\"width=device-width, initial-scale=1.0\">" + "\r\n");
			bfwriter.write(" <title>Informe de Pruebas Automatizado</title>" + "\r\n");
			bfwriter.write(
					" <meta name=\"description\" content=\"Demo que genera un archivo PDF con contenido HTML utilizando la libreria DOMPDF.\"/>"
							+ "\r\n");
			bfwriter.write(" <meta name=\"author\" content=\"Kenyi Nu�ez Velarde\"> " + "\r\n");
			bfwriter.write(" <link rel='stylesheet' href='css/font-awesome.min.css'> " + "\r\n");
			bfwriter.write(" <!-- Latest compiled and minified CSS --> " + "\r\n");
			bfwriter.write(
					" <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\"> "
							+ "\r\n");
			bfwriter.write(" <link rel=\"stylesheet\" href=\"ss/styles.css\"> " + "\r\n");
			bfwriter.write(" <script src=\"https://code.jquery.com/jquery-3.2.1.js\"></script>" + "\r\n");
			bfwriter.write(
					" <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js\"></script>"
							+ "\r\n");
			bfwriter.write(
					" <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js\"></script> "
							+ "\r\n");
			bfwriter.write(" <script type=\"text/javascript\">" + "\r\n");
			bfwriter.write(" var visto = null;" + "\r\n");
			bfwriter.write(" function ver(num) {" + "\r\n");
			bfwriter.write("   obj = document.getElementById(num);" + "\r\n");
			bfwriter.write("   obj.style.display = (obj==visto) ? 'none' : 'block';" + "\r\n");
			bfwriter.write("   if (visto != null)" + "\r\n");
			bfwriter.write("     visto.style.display = 'none';" + "\r\n");
			bfwriter.write("   visto = (obj==visto) ? null : obj;" + "\r\n");
			bfwriter.write(" }" + "\r\n");
			bfwriter.write(" </script>" + "\r\n");
			bfwriter.write(" </head>" + "\r\n");
			bfwriter.write(" <body>" + "\r\n");
			bfwriter.write(" <div class='container' >" + "\r\n");
			bfwriter.write("     <br><br>" + "\r\n");
			bfwriter.write("     <br><br>" + "\r\n");

			// TITULO DE REPORTE
			bfwriter.write("     <h2 align='center'>Informe de Pruebas Automatizado</h2><br>" + "\r\n");

			// CABECERA DEL REPORTE
			bfwriter.write(" <br/><br/><a>Sistema: </a>" + nombreSistema + "<br>" + "\r\n");
			bfwriter.write(" <a>Proceso: </a>" + nombreProceso + "<br>" + "\r\n");
			bfwriter.write(" <a>Fecha y hora inicio:</a> " + fechahoraInicio + "<br" + "\r\n");
			bfwriter.write(" <a>Fecha y hora Fin:</a> " + fechahoraFin + "<br" + "\r\n" + "\r\n");
			bfwriter.write("     <br><br>" + "\r\n");

			// RESUMEN DEL REPORTE
			bfwriter.write(" <strong>Resumen del Test</strong><br><br>" + "\r\n");
			bfwriter.write("     <div class=\"row\">" + "\r\n");
			bfwriter.write("         <div id=\"content\" class=\"col-lg-12\">" + "\r\n");
			bfwriter.write(
					"           <table width=\"500px\" cellpadding=\"5px\" cellspacing=\"5px\" border=\"1\" align=\"center\">"
							+ "\r\n");
			bfwriter.write(" <a>Total de escenarios ejecutados: </a>" + (int) pruebasejecutadas + " Escenarios" + "<br>"
					+ "\r\n");
			bfwriter.write(" <a>Total de escenarios OK: </a>" + (long) porcentajeOK + "% - " + (int) pruebasok
					+ " Escenarios" + "<br>" + "\r\n");
			bfwriter.write(" <a>Total de escenarios Fallidos: </a>" + (long) porcentajefalladas + "% - "
					+ (int) pruebasfalladas + " Escenarios" + "<br>" + "\r\n");
			// GRAFICO ESTAD�STICO
			bfwriter.write(" <canvas id=\"myChart\"></canvas>" + "\r\n");
			bfwriter.write(" <script src=\"chart.js\"></script>" + "\r\n");
			bfwriter.write(" <script>" + "\r\n");
			bfwriter.write(" var ctx = document.getElementById('myChart').getContext('2d');" + "\r\n");
			bfwriter.write(" var chart = new Chart(ctx, {" + "\r\n");
			bfwriter.write("     type: 'doughnut'," + "\r\n");
			bfwriter.write("     data:{" + "\r\n");
			bfwriter.write(" datasets: [{" + "\r\n");
			bfwriter.write(" data: [" + pruebasok + "," + pruebasfalladas + "," + pruebasinconclusas + "]," + "\r\n");
			bfwriter.write(" backgroundColor: ['green','red']," + "\r\n");
			bfwriter.write(" label: 'Comparacion de navegadores'}]," + "\r\n");
			bfwriter.write(" labels: ['Satisfactorios','Fallidos']}," + "\r\n");
			bfwriter.write("     options: {responsive: true}" + "\r\n");
			bfwriter.write(" });" + "\r\n");
			bfwriter.write(" </script>" + "\r\n");
			bfwriter.write(" </table>" + "\r\n");
			bfwriter.write("         </div>" + "\r\n");
			bfwriter.write("     </div><br>" + "\r\n");

			// DETALLE DEL TEST
			bfwriter.write(" <strong>Detalle del Test</strong><br><br>" + "\r\n");

			System.out.println("cantidad de registros" + Registros.size());
			for (RegistroFotos item : Registros) {
				bfwriter.write("<strong><p onclick=\"ver('" + item.getNumRegistro() + "\')\"> Escenario "
						+ item.numRegistro + " - " 
						+ "Estado: " + item.resultadoEjecucion + " - " + "Descripci�n del Estado: " + item.mensaje
						+ "</p></strong>" + "\r\n");

				bfwriter.write(" <p class=\"oculto\" id=\"" + item.getNumRegistro() + "\" align=\"center\">" + "\r\n");
				for (Imagen item2 : item.imagenes) {
					bfwriter.write("   &nbsp;&nbsp;Paso de prueba: " + item2.getNombrePrueba() + "<br><br>" + "\r\n");
					bfwriter.write("     <img src=\"" + item2.getRuta() + "\"/>" + "\r\n");
				}
				bfwriter.write(" </p>" + "\r\n");
			}

			bfwriter.write(" </p>" + "\r\n");
			bfwriter.write(" </div>" + "\r\n");
			bfwriter.write(" " + "\r\n");
			bfwriter.write(" </body>" + "\r\n");
			bfwriter.write(" </html>" + "\r\n");
			bfwriter.close();
			System.out.println("Archivo creado satisfactoriamente..");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (flwriter != null) {
				try {
					flwriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}