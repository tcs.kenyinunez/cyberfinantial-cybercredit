package Utilitario.Reporte;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.CellReference;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;

public class LeerExcel {
		
	private static HSSFWorkbook archivoExcel;

	/** 
    * Carga los datos de un excel a las pruebas
    * @param rutaArchivo Ruta donde se encuentra el archivo excel. Tener en cuenta que debe ser de formato .xls
    * @param filaInicioDatos Indica en que n�mero de fila comienzan los datos
    * @param cantidadColumnas Indica la cantidad de columnas a leer en el archivo input
    */  
	
	// CARGAR ARCHIVO EXCEL
	public String[][] Cargar(String rutaArchivo, int filaInicioDatos) throws IOException { 
			
    	SimpleDateFormat DateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss"); 		
		Date date = new Date();
    	String fechaInicio=DateFormat.format(date);    
		
		int contadorColumnas=0;
			String[][] lista = null; 
		
			File source = new File(rutaArchivo);		
			int i = 0;
			try {
				//TODO	
				FileInputStream archivo= new FileInputStream(new File(rutaArchivo));
				archivoExcel = new HSSFWorkbook(archivo);  
				HSSFSheet hojaExcel=archivoExcel.getSheetAt(0); //Hoja1 del excel  // SE CAMBIO A 1
				Iterator<Row> filas = hojaExcel.iterator(); //Obtengo un listado con todas las filas 
				
				int contador=1;				
				while(contador<filaInicioDatos) {
					filas.next(); //pasa a la segunda fila , porque a apartir de alli empieza los datos
					contador++;
				}
				int numeroFilas=0;
				
				if(filaInicioDatos>2)
					numeroFilas=hojaExcel.getLastRowNum()+1-(filaInicioDatos-1);
				else
					numeroFilas=hojaExcel.getLastRowNum();
					
					
				lista= new String[numeroFilas][];//creando un arreglo con un numero determiando de filas, .getLastRowNum -> indcia cuentas filas tiene (4 filas),segundo [] esta vacio porque las columnas son variables , pero las filas no
				
				DataFormatter formatter= new DataFormatter();
				while(filas.hasNext()){
					Row filaActual = filas.next(); 
					Iterator<Cell> celdas = filaActual.cellIterator(); //obteniendo todas las celdas de la fila actual
					lista[i]=new String[filaActual.getLastCellNum()];//indicando al arreglo la cantidad de columnas que va a tener
					int j=0; //j es columnas
					while(celdas.hasNext()){
						Cell celda = celdas.next();
						lista[i][j]=formatter.formatCellValue(celda.getRow().getCell(j)).toString().replace("'", ""); 
						j++;
					}
					i++; //i es fila
				
				}
			} catch (Exception e) {
				e.printStackTrace();
			} 
			 date = new Date() ;
		    	String fechaFin=DateFormat.format(date);    
			System.out.println("Cantidad de Registros: "+lista.length);
			return lista;
		}
	
	/**
	    * Actualiza datos de la PRIMERA HORA de un archivo excel
	    * @param fila Numero de Fila de la hoja excel
	    * @param nombreColumna Letra de columna de la hoja excel
	    * @param texto Texto a ingresar en la celda
	    * @param rutaArchivo Ruta del archivo excel a actualizar
	*/  
	public void ActualizarExcel(int fila, String nombreColumna,String texto, String rutaArchivo) {
		
		try {
			System.out.println("Actualizando archivo:" + rutaArchivo);
			//Read the spreadsheet that needs to be updated
			FileInputStream fsIP= new FileInputStream(new File(rutaArchivo));  
			//Access the workbook                  
			HSSFWorkbook wb = new HSSFWorkbook(fsIP);
			//Access the worksheet, so that we can update / modify it. 
			HSSFSheet worksheet = wb.getSheetAt(0); 
			// declare a Cell object
			Cell cell = null;  
			// Access the second cell in second row to update the value
			int columna =CellReference.convertColStringToIndex(nombreColumna);

			cell = worksheet.getRow(fila).getCell(columna);   
			
			if (cell == null || cell.getCellType() == Cell.CELL_TYPE_BLANK) {
				cell = worksheet.getRow(fila).createCell(columna);
			 }
			
			cell.setCellValue(texto);
			
			// Get current cell value value and overwrite the value
			
			//Close the InputStream  
			fsIP.close(); 
			//Open FileOutputStream to write updates
			FileOutputStream output_file =new FileOutputStream(new File(rutaArchivo));  
			 //write changes
			wb.write(output_file);
			//close the stream
			output_file.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
}
