package Utilitario.Reporte;

public class Imagen {
	
	 //Atributos de la clase
    public String nombrePrueba;
    public String ruta;
    
    public Imagen() {
		super();
	}
    
    public String getNombrePrueba() {
		return nombrePrueba;
	}
	public void setNombrePrueba(String nombrePrueba) {
		this.nombrePrueba = nombrePrueba;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
	  	this.ruta = ruta;
	}
    public Imagen(String nombrePrueba, String ruta) {
		super();
		this.nombrePrueba = nombrePrueba;
		this.ruta = ruta;
	}
}
