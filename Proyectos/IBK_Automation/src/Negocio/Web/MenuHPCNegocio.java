package Negocio.Web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.sikuli.script.FindFailed;

import Base.Selenium;
import Utilitario.Conecci�n.AutoIT_Clipboard;
import Utilitario.Conecci�n.InitSelenium;

public class MenuHPCNegocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private WebDriver driver;
	private Selenium accion;

	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public MenuHPCNegocio() {
		this.driver = InitSelenium.getDriver();
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public void ClicLinkOperaciones() throws InterruptedException {

		driver.findElement(By.xpath("//*[@id=\"WucMenuHpc_lblMenu\"]/table/tbody/tr/td[2]/a")).click();
		Actions builder = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"WucMenuHpc_lblMenu\"]/table/tbody/tr/td[2]/a"));
		builder.moveToElement(element).build().perform();

	}

	public void ClicLinkPagos() throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"021\"]/a[3]"));
		builder.moveToElement(element).build().perform();

	}

	public void ClicLinkPagosCuotas() throws InterruptedException {

		Actions builder = new Actions(driver);
		WebElement element = driver.findElement(By.xpath("//*[@id=\"021_0069\"]/a[3]"));
		builder.moveToElement(element).build().perform();

		driver.findElement(By.xpath("//*[@id=\"021_0069\"]/a[3]")).click();

	}
}
