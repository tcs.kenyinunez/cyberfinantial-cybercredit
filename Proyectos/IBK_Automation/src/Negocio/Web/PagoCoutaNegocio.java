package Negocio.Web;

import java.awt.AWTException;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.poi.ss.formula.ThreeDEval;
import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import Utilitario.Conecci�n.InitSelenium;
import interfazUsuario.NSAT_PagoCreditoUi;
import parametros.TpPagoCouta;

public class PagoCoutaNegocio {
	private AutoIt auto = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	public NSAT_PagoCreditoUi ui;
	public Properties properties = new Properties();
	public PagoCoutaNegocio() {
		this.ui = new NSAT_PagoCreditoUi();
	}
	public void login() throws InterruptedException {
		this.ui.cmbEntidad.waitAtImage();
		this.ui.cmbEntidad.click();
		Thread.sleep(500);
		auto.getAutoIt().send("{DOWN}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("{TAB}", false);
//		this.ui.cmbIndexInterbank.click();
		this.ui.btnAceptar.click();
	}
	public void buscarContrato(TpPagoCouta caso) throws Exception {
		Thread.sleep(3000);
		this.ui.lblSistemaPago.waitAtImage();
		this.ui.lblSATMenu.waitAtImage();
		this.ui.lblSATMenu.hover();
		Thread.sleep(1000);
		this.ui.lblGestionContratosyTarjetas.waitAtImage();
		this.ui.lblGestionContratosyTarjetas.click();
		Thread.sleep(1000);
		this.ui.lblBusquedaContratos.click();
		Thread.sleep(2000);
		this.ui.txtNroContrato.click();
		Thread.sleep(500);
		auto.getAutoIt().send(caso.getNroCuenta().substring(1,13));
		Thread.sleep(500);
		this.ui.btnBuscar.click();
		this.ui.lblLoadingInfo.waitInvisibility();
		this.ui.btnOpciones.waitAtImage();
		this.ui.btnOpciones.hover();
		this.ui.lblInclusionPageExterno.click();
		Thread.sleep(1000);
		this.ui.lblInclusionPageExternoTitle.waitAtImage();
		Thread.sleep(1000);
		auto.getAutoIt().send(caso.getImporte());
		Thread.sleep(1000);
		auto.getAutoIt().send("{TAB}",false);
		switch(caso.getMoneda()) {
		case "001":
		case "Nuevo Sol":
		case "nuevo sol":{
			caso.setMoneda("NUEVO SOL");
			Thread.sleep(1000);
			auto.getAutoIt().send("{down}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "010":
		case "dolar":
		case "Dolar":{
			caso.setMoneda("DOLAR");
			Thread.sleep(1000);
			auto.getAutoIt().send("{down}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{down}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		default:{
			throw new Exception("No se ha reconocido la moneda : " + caso.getMoneda());
		}
		}
		this.ui.txtFecha.sendKeys(caso.getFecha());
		this.ui.cmbTipo.click();
		switch(caso.getTipo()) {
			case "cheque":
			case "Cheque":{
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
//				this.ui.cmbTipoIndexCheque.click();
				break;
			}
			case "giro":
			case "Giro":{
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
//				this.ui.cmbTipoIndexGiro.click();
				break;
			}
			case "metalico":
			case "Metalico":{
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
//				this.ui.cmbTipoIndexMetalico.click();
				break;
			}
			case "internet":
			case "Internet":{
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
//				this.ui.cmbTipoIndexInternet.click();
				break;
			}
			case "Otros medios":
			case "otros medios":{
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
				Thread.sleep(1000);
				auto.getAutoIt().send("{down}",false);
//				this.ui.cmbTipoIndexOtros.click();
				break;
			}
			default:{
				throw new Exception("No se reconocio el tipo ingresado : " + caso.getTipo());
			}
		}
		auto.getAutoIt().send("{ENTER}",false);
		this.ui.icoLupa.click();
		this.ui.lblAbonoRegular.click();
		this.ui.btnConfirmar.click();
		this.ui.btnOk.click();
		Thread.sleep(2000);
		this.ui.btnOk.waitAtImage();
		this.ui.btnOk.click();
		Thread.sleep(1000);
		this.ui.lblLoadingInfo.waitInvisibility();
		this.ui.btnOpciones.waitAtImage();
		this.ui.btnOpciones.hover();
		Thread.sleep(1000);
		this.ui.lblPosicionEconomicaContrato.click();
		this.ui.lblPosicionEconomicaContratoTitle.waitAtImage();
		Thread.sleep(2000);
		this.ui.btnMas.waitAtImage();
		this.ui.btnMas.click();
		Thread.sleep(1000);
		this.ui.btnMasCredito.click();
		Thread.sleep(3000);
		this.ui.lblMonedaTable.waitAtImage();
		String monedaObtenida = captureCell(this.ui.lblMonedaTable,1,75);
		if(monedaObtenida.equalsIgnoreCase(caso.getMoneda())) {
			this.ui.btnPrimerResultMas.captureSizeElement();
			auto.getAutoIt().mouseClick("left", this.ui.btnPrimerResultMas.xInicio, this.ui.btnPrimerResultMas.yInicio + this.ui.btnPrimerResultMas.alturaCelda);
			Thread.sleep(2000);
			this.captureDataTableResult(caso,this.ui.lblFechaOperacionTable,this.ui.lblAnuladaTable);
		}else {
			monedaObtenida = captureCell(this.ui.lblMonedaTable, 2, 75);
			this.ui.btnSegundoResultMas.captureSizeElement();
			auto.getAutoIt().mouseClick("left", this.ui.btnSegundoResultMas.xInicio, this.ui.btnSegundoResultMas.yInicio + this.ui.btnSegundoResultMas.alturaCelda);
			Thread.sleep(2000);
			this.captureDataTableResult(caso,this.ui.lblFechaOperacionTable,this.ui.lblAnuladaTable);
		}
	}
	public String captureCell(Sikulix element,int ordenNumero, int porcentaje) throws AWTException, IOException, FindFailed, InterruptedException {
		element.captureSizeElement();
		String ruta = element.captureCell(element.xInicio, element.yInicio + (ordenNumero * element.alturaCelda),
				element.longitudCelda + element.alturaCelda, (element.alturaCelda * porcentaje/100), 1);
		System.out.println("Ruta para la imagen guardada " + ruta);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strCapture = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
		if(strCapture.equals("D0LAR")) {
			strCapture = "Dolar";
		}else if(strCapture.equals("NUEV0S0L")) {
			strCapture = "Nuevo Sol";
		}
		else if(strCapture.equals("PEHDIEITE")) {
			strCapture = "Pendiente";
		}
		return strCapture;
	}
	public void captureDataTableResult(TpPagoCouta caso ,Sikulix elementStart,Sikulix elementEnd) throws FindFailed, InterruptedException, AWTException, IOException {
		Thread.sleep(2000);
		elementStart.waitAtImage();
		elementEnd.waitAtImage();
		elementStart.captureSizeElement();
		elementEnd.captureSizeElement();
		System.out.println(elementStart.toString());
		int AxisXStart = elementStart.xInicio - elementStart.xInicio/20;
		int AxisYStart = elementStart.yInicio + elementStart.alturaCelda + (elementStart.alturaCelda/2);
		int AxisXEnd = elementEnd.xFinal;
		int AxisYEnd = elementEnd.yInicio + elementEnd.alturaCelda + (elementEnd.alturaCelda /2);
		auto.getAutoIt().mouseMove(AxisXStart, AxisYStart);
		auto.getAutoIt().mouseClickDrag("left", AxisXStart, AxisYStart, AxisXEnd, AxisYEnd);
		auto.getAutoIt().send("^c",false);
		Thread.sleep(2000);
		String lastResult = getLastDataTable(auto.extraerPortapeles());
		System.out.println(lastResult);
		List<String> listResult = Arrays.asList(lastResult.split("  ")).stream()
			.filter((line) -> {
				return !line.equals("");
			})
			.collect(Collectors.toList());
		String fecha = listResult.stream().findFirst().get().trim();
		String importe = listResult.get(3).trim().replace(".","").replace(",", ".");
		String moneda = listResult.get(4).trim();
		System.out.println(fecha + importe + moneda);
		if (fecha.equals(caso.getFecha()) && importe.equals(caso.getImporte()) && moneda.equalsIgnoreCase(caso.getMoneda())) {
			System.out.println(caso.getNroCorrelativo());
			TpPagoCouta.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, "Validado");
		}else {
			TpPagoCouta.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, "Incorrecto");
		}
	}
	public String getLastDataTable(String contentTable){
		//JAVA 8 *o*
		List<String> listResult = Arrays.asList(contentTable.split("\n")).stream()
				.filter(line -> !line.trim().startsWith("Mostrar"))
				.filter(line -> !line.trim().startsWith("Compra"))
				.filter(line -> !line.trim().startsWith("Impagados"))
				.filter(line -> !line.equals(" "))
				.collect(Collectors.toList());
		long count = listResult.stream().count();
		return listResult.stream().skip(count - 1).findFirst().get();
	}
	
	public void pagarCuotaHPC(TpPagoCouta caso) throws Exception{
		
		/*-----------------INGRESO A HPC--------------------*/
		InitSelenium initSelenium = new InitSelenium();
	
			properties.load(new FileReader(".\\properties\\general.properties"));
			
		
			//accion = new Selenium();
			LoginHPCNegocio loginHPCPage = new LoginHPCNegocio();
			MenuHPCNegocio menuHPCPage = new MenuHPCNegocio();
			PagoCuotasHPCNegocio pagoCuotasPage = new PagoCuotasHPCNegocio();
			//accion = new Selenium();
			
			// Logeo
			loginHPCPage.ui.IngresarUsuarioHPC(properties.getProperty("user"));
			
			loginHPCPage.ui.IngresarClaveHPC(properties.getProperty("pass"));
			loginHPCPage.ui.SeleccionarAmbienteHPC();
			loginHPCPage.ui.ClicAceptarLogin();
			Thread.sleep(2000);
			loginHPCPage.ui.ClicLink();
	
			// Pago de cuotas
			Thread.sleep(9000);
			menuHPCPage.ClicLinkOperaciones();
			Thread.sleep(1000);
			menuHPCPage.ClicLinkPagos();
			Thread.sleep(1000);
			menuHPCPage.ClicLinkPagosCuotas();
			pagoCuotasPage.ClicNuevo();
			pagoCuotasPage.IngresarCredito(caso.getNroCuenta());
			Thread.sleep(6000);
			pagoCuotasPage.MarcarCuota();
			pagoCuotasPage.IngresarImportePagar(caso.getImporte());
			pagoCuotasPage.IngresarMedioCargo(caso.getMedioCargo());
			pagoCuotasPage.IngresarMotivo(caso.getMotivo());
			Thread.sleep(2000);
			pagoCuotasPage.Guardar();
			Thread.sleep(1000);
			pagoCuotasPage.OK();
			pagoCuotasPage.ValidarPagoRealizado();
			TpPagoCouta.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, "Validado");
		
	}
}
