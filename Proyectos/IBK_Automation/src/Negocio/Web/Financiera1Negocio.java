package Negocio.Web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;

import Base.Selenium;
import Utilitario.Conecci�n.InitSelenium;

public class Financiera1Negocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private WebDriver driver;
	private Selenium accion;

	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public Financiera1Negocio() {
		driver = InitSelenium.getDriver();
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public void ClicFinanciera1() throws FindFailed, InterruptedException {
		accion.ClickAImagen("Resources\\Financiera1.png");
	}

}