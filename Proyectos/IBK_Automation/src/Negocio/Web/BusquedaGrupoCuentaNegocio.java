package Negocio.Web;

import org.sikuli.script.FindFailed;

import Base.Selenium;

public class BusquedaGrupoCuentaNegocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private Selenium accion;

	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public BusquedaGrupoCuentaNegocio() {
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public void ClicBusquedaGrupoCuenta() throws FindFailed, InterruptedException {
		accion.ClickAImagen("Resources\\botonBusquedaNumeroCuenta.png");
	}
	public void IngresoBusquedaGrupoCuenta(String strNumCuenta) throws FindFailed, InterruptedException {
		accion.SendKeysAImagen("Resources\\IngresoBusquedaNumeroCuenta.png", strNumCuenta);
	}
	public void BuscarBusquedaGrupoCuenta() throws FindFailed, InterruptedException {
		accion.ClickAImagen("Resources\\Buscar.png");
	}
	public void BuscarCliente(String strNumCuenta) throws FindFailed, InterruptedException {
		this.ClicBusquedaNroCliente();
		this.IngresoBusquedaGrupoCuenta(strNumCuenta);
		this.BuscarBusquedaGrupoCuenta();
	}
	public void ClicBusquedaNroCliente() throws FindFailed, InterruptedException {
		accion.ClickAImagen("Resources\\btnBusquedaNroCliente.jpg");
	}
	public void IngresoBusquedaNroCliente(String strNumCuenta) throws FindFailed, InterruptedException {
		accion.SendKeysAImagen("Resources\\\\btnBusquedaNroCliente.jpg", strNumCuenta);
	}

}
