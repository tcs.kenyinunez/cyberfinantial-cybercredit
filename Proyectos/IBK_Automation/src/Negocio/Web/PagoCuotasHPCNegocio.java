package Negocio.Web;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;

import Utilitario.Conecci�n.AutoIT_Clipboard;
import Utilitario.Conecci�n.InitSelenium;
import autoitx4java.AutoItX;
import interfazUsuario.PagoCuotaHPCUi;
import Base.AutoIt;
import Base.Selenium;

public class PagoCuotasHPCNegocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private WebDriver driver;
	private Selenium accion;
	public PagoCuotaHPCUi ui;
	
	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public PagoCuotasHPCNegocio() {
		this.driver = InitSelenium.getDriver();
		accion = new Selenium();
		this.ui = new PagoCuotaHPCUi();
	}

	// PASOS DE PRUEBAS
	public void ClicNuevo() throws FindFailed, InterruptedException {
		Thread.sleep(1000);
		//accion.ClickAImagen("Resources\\FechaProceso.png");
		WebElement frmContent = driver.findElement(By.id("frmcontent"));
		driver.switchTo().frame(frmContent);
		driver.findElement(By.id("fpAnimswapImgFP1")).click();
	}
	public void IngresarCredito(String strCredito) throws FindFailed, InterruptedException, AWTException {
		strCredito = strCredito.substring(4,9);
		System.out.println("Corte: " + strCredito);
		this.ui.gettxtCodigo().sendKeys(strCredito);
//		AutoIT_Clipboard svrs = new AutoIT_Clipboard();
//		svrs.getAutoIt().send("{ENTER}",false);
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
	}
	
	public void MarcarCuota() throws FindFailed, InterruptedException {
		this.ui.getChkSeleccion().click();
		//driver.findElement(By.id("dgLista_ctl03_chkSeleccion")).click();
	}
	

	public void IngresarImportePagar(String strImportePagar) {
		this.ui.getTxtPago().clear();
		this.ui.getTxtPago().click();
		this.ui.getTxtPago().sendKeys(strImportePagar);
	}
	
	public void IngresarMedioCargo(String strMedioCargo) {
		this.ui.getListCuentaContable().sendKeys(strMedioCargo);
	}

	public void IngresarMotivo(String strMotivo) {
		this.ui.getListMotivo().sendKeys(strMotivo);
	}

	public void Guardar() {
		this.ui.getBtnGuardar().click();
	}

	public void OK() throws FindFailed, InterruptedException, AWTException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ENTER);
	}
	
	public void ValidarPagoRealizado() throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(By.xpath("//*[@id=\"dgrdCred\"]/tbody/tr[2]/td[5]")));
		driver.findElement(By.xpath("//*[@id=\"dgrdCred\"]/tbody/tr[2]/td[5]")).click();
		String a = driver.findElement(By.xpath("//*[@id=\"dgrdCred\"]/tbody/tr[2]/td[5]")).getText();
		System.out.println("Pago realizado: " + a);
	}
}
