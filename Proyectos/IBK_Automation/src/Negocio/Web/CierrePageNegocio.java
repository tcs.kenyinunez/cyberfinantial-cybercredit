package Negocio.Web;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;
import java.io.IOException;
import org.sikuli.script.FindFailed;

import Utilitario.Conecci�n.AutoIT_Clipboard;
import interfazUsuario.CierreUi;
import Base.Selenium;

public class CierrePageNegocio{

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private Selenium accion;
	public CierreUi ui;
	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public CierrePageNegocio() {
		accion = new Selenium();
		this.ui = new CierreUi();
	}

	// PASOS DE PRUEBAS
	public void ClicCierreVentana() throws FindFailed, InterruptedException, AWTException {
//		accion.ClickAImagen("Resources\\CloseWindows.png");
		Robot robot= new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_F4);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_F4);
	}
	public void ClickSalida() throws FindFailed, InterruptedException {
		this.ui.btnSalida.click();
	}
	public void ClicOKCierre() throws FindFailed, InterruptedException {
		this.ui.OkSalida.click();
	}
	public void ClicCierreVentanaCyber() throws FindFailed, InterruptedException {
		this.ui.CerrarVentanaCyber.click();
	}

}
