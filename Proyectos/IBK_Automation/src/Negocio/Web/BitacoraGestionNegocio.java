package Negocio.Web;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;

public class BitacoraGestionNegocio {
	private static Selenium accion;
	private TesseractOCR tesseractOCR = new TesseractOCR();
	public Sikulix lblComercial;
	public AutoIt auto = new AutoIt();
	
	public BitacoraGestionNegocio() {
		accion = new Selenium();
	}
	
	public String validarBitacora() throws AWTException, IOException, FindFailed, InterruptedException {
		this.getBitacoraGestion().waitAtImage();
		this.getBitacoraGestion().click();
		Thread.sleep(1000);
		this.getLblComentario().waitAtImage();
		this.getLblComentario().captureSizeElement();
		System.out.println(getLblComentario().toString());
		String ruta = this.getLblComentario().captureCell(getLblComentario().xInicio+2, getLblComentario().yInicio + getLblComentario().alturaCelda + 1,
				getLblComentario().longitudCelda + getLblComentario().alturaCelda, getLblComentario().alturaCelda - 1, 1);
		System.out.println("ruta de Imagen : " + ruta);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String celda1 = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
		celda1 = celda1.replace("Promaa", "Promesa");
		System.out.println("Valor del comentario: " +celda1);
		String comentario = this.auto.getAutoIt().clipGet();
		System.out.println("Copiado del comentario: " + comentario.replace(" ",""));
		if(comentario.replace(" ","").equals(celda1)) {
			return "Validado";
		}else {
			return "Incorrecto";
		}
	}
	
	public Sikulix getLblComentario() {
		if (lblComercial==null) {
			return lblComercial = new Sikulix("\\Resources\\bitacoraGestionPage\\lblComentario.PNG");
		}
		return lblComercial;
	}
	public Sikulix getBitacoraGestion() {
		return new Sikulix("\\Resources\\bitacoraGestionPage\\lblGestionBitacora.PNG");
	}
}
