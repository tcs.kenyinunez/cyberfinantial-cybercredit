package Negocio.Web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Utilitario.Conecci�n.InitSelenium;
import interfazUsuario.LoginCyberCreditUi;

public class LoginCyberCreditNegocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private WebDriver driver;
	private Selenium accion;
	public LoginCyberCreditUi ui = new LoginCyberCreditUi();
	public AutoIt auto = new AutoIt();
	
	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public LoginCyberCreditNegocio() {
		driver = InitSelenium.getDriver();
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public void ClicCyberCredit() {
		driver.findElement(By.xpath("/html/body/div/table/tbody/tr/td[1]/a/img")).click();
		auto.getAutoIt().winWait("http://s417va7:9080/bei010Web/bei010/bei10.jsp - Internet Explorer");
		auto.getAutoIt().winActivate("http://s417va7:9080/bei010Web/bei010/bei10.jsp - Internet Explorer");
	}
	public void IngresarNomUsuario(String strNomUsuario) throws FindFailed, InterruptedException {
		Thread.sleep(1000);
		this.ui.txtUsuario.sendKeys(strNomUsuario);
	}
	public void IngresarNomContrasena(String strNomContrasena) throws FindFailed, InterruptedException {
		Thread.sleep(1000);
		this.ui.txtContrasena.sendKeys(strNomContrasena);
	}
	public void ClicOK() throws FindFailed, InterruptedException {
		this.ui.btnOk.click();
	}
	public void Login(String strNomUsuario, String strNomContrasena) throws FindFailed, InterruptedException {
		Thread.sleep(1000);
		this.ClicCyberCredit();
		Thread.sleep(1000);
		this.IngresarNomUsuario(strNomUsuario);
		Thread.sleep(1000);
		this.IngresarNomContrasena(strNomContrasena);
		this.ClicOK();
	}
//	public String ObtenerHorario() {
//		String Valorgrid = driver.findElement(By.xpath("/html/body/footer/div")).getText().substring(45,53);
//		System.out.println("Numero de Relaci�n:" + Valorgrid);
//		return Valorgrid;
//	}

}
