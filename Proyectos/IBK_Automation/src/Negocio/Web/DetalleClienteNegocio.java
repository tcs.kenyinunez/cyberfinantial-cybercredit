package Negocio.Web;

import java.awt.AWTException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import interfazUsuario.DetalleClienteUi;
import parametros.TpCyberCreditPromesasPago;

public class DetalleClienteNegocio {

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private static Selenium accion;
	private AutoIt auto = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	
	public DetalleClienteUi ui = new DetalleClienteUi();

	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public DetalleClienteNegocio() {
		accion = new Selenium();
	}
	
	// PASOS DE PRUEBAS
	public static void main(String filePath) throws FindFailed, InterruptedException {
		TesseractOCR my_main = new TesseractOCR();
		if (my_main.strCuenta == "" || my_main.strCuenta == null ) {
			System.out.println("Error");
		}else {
		accion.DoubleClickAImagen(filePath);
		Thread.sleep(4000);
		}
	}	
	
	
	public static Sikulix cmbMoneda = new Sikulix("");
	
	public void crearPromesaPago(TpCyberCreditPromesasPago caso) throws FindFailed, InterruptedException {
		Thread.sleep(2000);
		this.ui.btnOkError.waitAtImage();
		this.ui.btnOkError.click();
		this.ui.getTxtCA().waitAtImage();
		this.ui.getTxtCA().sendKeys("LD");
		this.ui.getTxtCR().waitAtImage();
		this.ui.getTxtCR().sendKeys("PP");
		Thread.sleep(1000);
		this.ui.getTxtMonto().waitAtImage();
		this.ui.getTxtMonto().sendKeys(caso.getMonto());
		this.ui.getTxtFechaVencimiento().waitAtImage();
		Calendar c1 = Calendar.getInstance();
		c1.add(Calendar.DAY_OF_MONTH, 7);
		String today = c1.get(Calendar.DAY_OF_MONTH) + "-" + c1.get(Calendar.MONTH) + 1 + "-" + c1.get(Calendar.YEAR);
		this.ui.getTxtFechaVencimiento().sendKeys(today);
		Thread.sleep(1000);
		this.auto.getAutoIt().send("{TAB}",false);
		Thread.sleep(1000);
		this.auto.getAutoIt().send("{TAB}",false);
		Thread.sleep(1000);
		this.auto.getAutoIt().send("+{PGDN}",false);
		Thread.sleep(1000);
		this.auto.getAutoIt().send("^c",false);
		Thread.sleep(1000);
		this.ui.getTxtArea().waitAtImage();
		this.ui.getTxtArea().sendKeys(caso.getArea());
		Thread.sleep(1000);
		this.ui.getTxtTelefono().waitAtImage();
		this.ui.getTxtTelefono().sendKeys(caso.getTelefono());
		Thread.sleep(1000);
		this.ui.getBtnGuardar().waitAtImage();
		this.ui.getBtnGuardar().click();
	}
	public void buscarPorCredito() throws FindFailed, InterruptedException, AWTException, IOException {
		this.ui.getLblCuenta().resizeElement(20);
		this.ui.lblCuenta.captureSizeElement();
		for(int times = 1; times<3;times++) {
			String ruta = this.ui.lblCuenta.captureCell(this.ui.lblCuenta.xInicio, this.ui.lblCuenta.yInicio + (times*this.ui.lblCuenta.alturaCelda), this.ui.lblCuenta.longitudCelda + this.ui.lblCuenta.alturaCelda,this.ui.lblCuenta.alturaCelda ,times);
			Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
			String celda1 = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
			if (celda1.equals("CJ46133090$CL001")) {
				Thread.sleep(1000);
				auto.getAutoIt().send("{TAB}", false);
				Thread.sleep(1000);
				auto.getAutoIt().send("+{TAB}", false);
				Thread.sleep(1000);
				for(int index=0;index<times;index++) {
					auto.getAutoIt().send("{DOWN}",false);
					Thread.sleep(1000);
				}
				auto.getAutoIt().send("{ENTER}", false);
				Thread.sleep(1000);
				break;
			}
		}
	}
}
