package Negocio.Web;

import java.awt.AWTException;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import Negocio.Web.Financiera2Negocio.Coutas;
import Utilitario.Excel;
import interfazUsuario.ComprobarStatusUi;
import parametros.TpComprobarStatus;

public class ComprobarStatusNegocio {
	private AutoIt auto = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	public ComprobarStatusUi ui = new ComprobarStatusUi();
	public MaestraNegocio maestra = new MaestraNegocio();
	public DemograficaNegocio demograficaPage = new DemograficaNegocio();
	public Financiera1Negocio f1 = new Financiera1Negocio();
	public Financiera2Negocio f2 = new Financiera2Negocio();
	
	public void validarStatusR(TpComprobarStatus caso) throws InterruptedException, FindFailed, AWTException, IOException, ClassNotFoundException, UnsupportedFlavorException {
		this.ui.btnBusquedaporNumCuenta.click();
		Thread.sleep(1000);
		this.ui.txtBusquedaporNumCuenta.sendKeys(caso.getCuenta());
		this.ui.btnBuscar.click();
		Thread.sleep(1000);
		auto.getAutoIt().send("{ENTER}",false);
		Thread.sleep(1000);
		obtenerDatosCuenta(caso);
		validarEstadoAdeudos(5);
	}
	public void obtenerDatosCuenta(TpComprobarStatus caso) throws IOException, InterruptedException {
		int indRow = caso.getNroCorrelativo() + 4;
		this.ui.lblDetalleCliente.click();
		Thread.sleep(500);
		auto.getAutoIt().send("{TAB}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("+{END}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("^c",false);
		Thread.sleep(500);
		caso.setCliente(auto.getAutoIt().clipGet());
		auto.getAutoIt().send("{TAB}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("+{END}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("^c",false);
		caso.setNombre(auto.getAutoIt().clipGet());
		auto.getAutoIt().send("{TAB}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("+{END}",false);
		Thread.sleep(500);
		auto.getAutoIt().send("^c",false);
		caso.setDireccion(auto.getAutoIt().clipGet());
		TpComprobarStatus.establecerValorCelda(indRow, 2, caso.getCliente());
		TpComprobarStatus.establecerValorCelda(indRow, 3, caso.getNombre());
		TpComprobarStatus.establecerValorCelda(indRow, 4, caso.getDireccion());
	}
	public String captureStatusR() throws FindFailed, InterruptedException, AWTException, IOException {
		this.ui.lblAgencia.captureSizeElement();
		Thread.sleep(500);
		auto.getAutoIt().mouseClickDrag("left", this.ui.lblAgencia.xFinal, this.ui.lblAgencia.yInicio + (this.ui.lblAgencia.alturaCelda*5/4),
				this.ui.lblAgencia.xFinal-this.ui.lblAgencia.longitudCelda, this.ui.lblAgencia.yInicio + (this.ui.lblAgencia.alturaCelda*5/4));
		auto.getAutoIt().send("^c",false);
		String status = auto.getAutoIt().clipGet();
		Thread.sleep(500);
		if (status.equals("R")) {
			return "R";
		}else {
			return "null";
		}
	}
	public void validarEstadoAdeudos(int numberRows) throws InterruptedException, FindFailed, AWTException, IOException, ClassNotFoundException, UnsupportedFlavorException{
		boolean ultimoencontrado=false;
		String strCuenta = "";
		String strStatus = "";
		String strSaldo = "";
		String strMoneda = "";
		String strCuentaAnterior="";
		int registro = 1;
		while(!ultimoencontrado) {
			this.ui.lblCuentaTable.captureSizeElement();
			this.ui.lblCuentaTable.resizeElement(20);
			if(registro==1) {
				this.ui.btnOkError.waitAtImage();
				this.ui.btnOkError.click();
			}
//			auto.getAutoIt().send("{ENTER}",false);
			Thread.sleep(1000);
			this.ui.lblCuentaTable.captureSizeElement();
			this.ui.lblStatusTable.captureSizeElement();
			this.ui.lblSaldoVencidoTable.captureSizeElement();
			this.ui.lblMonedaTable.captureSizeElement();
			for(int timesTAB=0;timesTAB<19;timesTAB++) {
				auto.getAutoIt().send("{TAB}",false);
				Thread.sleep(100);
			}
			for(int timesTAB=0;timesTAB<2;timesTAB++) {
				auto.getAutoIt().send("{TAB}",false);
				Thread.sleep(100);
			}
			this.ui.lblCuentaTableResize.captureSizeElement();
			this.ui.lblStatusTable.captureSizeElement();
			this.ui.lblSaldoVencidoTable.captureSizeElement();
			this.ui.lblMonedaTable.captureSizeElement();
			for(int x=0;x<registro;x++) {
				auto.getAutoIt().send("{DOWN}",false);
				Thread.sleep(500);
			}
			if(registro<5) {
				strCuenta = captureValueCellOnlyNumber(this.ui.lblCuentaTableResize, registro);
				strStatus = captureValueCellOnlyLetters(this.ui.lblStatusTable, registro);
				strSaldo = captureValueCell(this.ui.lblSaldoVencidoTable, registro);
				strMoneda = captureValueCell(this.ui.lblMonedaTable, registro);	
			}else {
				strCuenta = captureValueCellOnlyNumber(this.ui.lblCuentaTableResize, 5);
				strStatus = captureValueCellOnlyLetters(this.ui.lblStatusTable, 5);
				strSaldo = captureValueCell(this.ui.lblSaldoVencidoTable, 5);
				strMoneda = captureValueCell(this.ui.lblMonedaTable, 5);
			}
			if(strCuentaAnterior.equals(strCuenta) || strCuenta.equals("")) {
				ultimoencontrado=true;
				break;
				
			}else {
				auto.getAutoIt().send("{ENTER}",false);
				verificarDentroCuenta(strCuenta, strStatus, strSaldo, strMoneda);
			}
			if(!strCuenta.trim().equals("") && !strStatus.trim().equals("") && !strSaldo.trim().equals("")) {
				if (strStatus.substring(strStatus.length()-1,strStatus.length()).equals("R")) {
					System.out.println("Cuenta " + strCuenta + " con Status R");
				}else {
					System.out.println("Cuenta " + strCuenta + " con Status null");
				}
			}else {
				System.out.println("Se encontro el ultimo registro...");
				ultimoencontrado = true;
				break;
			}
			if(ultimoencontrado) {
				break;
			}
			strCuentaAnterior = strCuenta;
			registro++;
		}
		System.out.println("***Metodo Terminado Status***");
	}
	public String  captureValueCell(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio +2, element.yInicio +1 +(x * element.alturaCelda),
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	public String  captureValueCellOnlyNumber(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio + 4, element.yInicio + (x * element.alturaCelda),
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImageOnlyNumber(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	public String  captureValueCellOnlyLetters(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio, element.yInicio + (x * element.alturaCelda) + 1,
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImageOnlyLetters(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	public void verificarDentroCuenta(String cuenta, String status, String saldo, String moneda) throws InterruptedException, FindFailed, ClassNotFoundException, UnsupportedFlavorException, IOException, AWTException {
		Thread.sleep(1000);
		maestra.ValidarGrupo(cuenta.substring(0,1));
		maestra.ValidarCuenta(cuenta);
		maestra.ValidarMontoVencido(saldo);
		demograficaPage.ClicDemografica();
		Thread.sleep(1000);
		captureStatusR();
		f1.ClicFinanciera1();
		Thread.sleep(1000);
		captureStatusR();
		Thread.sleep(1000);
		f2.ClicFinanciera2();
		Thread.sleep(1000);
		captureStatusR();
		Thread.sleep(1000);
		if(cuenta.substring(0,1).equals("6")) {
			f2.validarHistoricoCuotas(6);
			f2.validarHistoricoPagos(23);
		}else if(cuenta.substring(0,1).equals("4")){
			f2.validarHistoricoPagos(16);
		}else {
			f2.validarHistoricoCuotas(13);
			f2.validarHistoricoPagos(13);
		}
		Thread.sleep(1000);
		this.ui.txtCA.sendKeys("Mm");
		Thread.sleep(2000);
	}
	
}
