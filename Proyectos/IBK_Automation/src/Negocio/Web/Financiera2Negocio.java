package Negocio.Web;

import java.awt.AWTException;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.sikuli.script.FindFailed;

import Utilitario.Excel;
import Utilitario.Conecci�n.AutoIT_Clipboard;
import interfazUsuario.Financiera2Ui;
import parametros.TpPagoCouta;
import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;

public class Financiera2Negocio{
	public Financiera2Ui ui = new Financiera2Ui();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	private Properties properties = new Properties();
	public AutoIt auto = new AutoIt();
	class Coutas{
		String nro;
		String Moneda;
		String Importe;
		Coutas(String nro,String moneda, String Importe){
			this.nro = nro;
			this.Moneda = moneda;
			this.Importe = Importe;
		}
		public String getNro() {
			return nro;
		}
		public String getMoneda() {
			return Moneda;
		}
		public String getImporte() {
			return Importe;
		}
	}
	class Pagos{
		String operacion;
		String fechaPago;
		String mtoPago;
		String tipPago;
		Pagos(String operacion,String fechaPago, String mtoPago, String tipPago){
			this.operacion = operacion;
			this.fechaPago = fechaPago;
			this.mtoPago = mtoPago;
			this.tipPago = tipPago;
		}
		public String getOperacion() {
			return operacion;
		}
		public void setOperacion(String operacion) {
			this.operacion = operacion;
		}
		public String getFechaPago() {
			return fechaPago;
		}
		public void setFechaPago(String fechaPago) {
			this.fechaPago = fechaPago;
		}
		public String getMtoPago() {
			return mtoPago;
		}
		public void setMtoPago(String mtoPago) {
			this.mtoPago = mtoPago;
		}
		public String getTipPago() {
			return tipPago;
		}
		public void setTipPago(String tipPago) {
			this.tipPago = tipPago;
		}
		
	}
	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private Selenium accion;
	
	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public Financiera2Negocio() {
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public void ClicFinanciera2() throws FindFailed, InterruptedException {
		accion.ClickAImagen("Resources\\Financiera2.png");
	}
	
	public void validarHistoricoCuotas(int numberRows) throws FindFailed, InterruptedException, AWTException, IOException{
		boolean ultimoencontrado=false;
		int pasadas = 0;
		ArrayList<Coutas> arrayCoutas = new ArrayList<Coutas>();
		while(!ultimoencontrado) {
			this.ui.lblFinancieraTwoTitle.click();
			auto.getAutoIt().send("{TAB}",false);
			Thread.sleep(100);
			auto.getAutoIt().send("+{END}",false);
			Thread.sleep(100);	
			auto.getAutoIt().send("^c",false);
			String cuenta = auto.getAutoIt().clipGet();
			auto.getAutoIt().send("{TAB}",false);
			Thread.sleep(100);
			auto.getAutoIt().send("+{END}",false);
			Thread.sleep(100);	
			auto.getAutoIt().send("^c",false);
			cuenta += auto.getAutoIt().clipGet();
			this.ui.lblNroTable.captureSizeElement();
			this.ui.lblMonedaTable.captureSizeElement();
			this.ui.lblImporteTotalTable.captureSizeElement();
			for(int timesClick=0;timesClick<7;timesClick++) {
				Thread.sleep(500);
				auto.getAutoIt().send("{TAB}",false);
			}
			//Primera barrida de registros
			for(int index=0;index<numberRows;index++) {
				Thread.sleep(300);
				auto.getAutoIt().send("{DOWN}",false);
			}
			for(int x=1;x<numberRows+1;x++) {
				String strNro = captureValueCellOnlyNumber(this.ui.lblNroTable, x);
				String strMoneda = captureValueCell(this.ui.lblMonedaTable, x);
				String strImporte = captureValueCell(this.ui.lblImporteTotalTable, x);
				if(!strNro.trim().equals("") && !strMoneda.trim().equals("") && !strImporte.trim().equals("")) {
					String ultimocharImporte = strImporte.substring(strImporte.length()-1, strImporte.length());
					String filtroImporte="";
					int indexChar=0;
					if(strMoneda.contains("LES")) {
						strMoneda = "SOLES";
					}else if(strMoneda.contains("RES")){
						strMoneda = "DOLARES";
					}
					for(char charImporte: strImporte.toCharArray()) {
						if(Character.isDigit(charImporte)) {
							filtroImporte += charImporte;
						}
						if(indexChar==strImporte.length()-3) {
							filtroImporte += ".";
						}
						indexChar++;
					}
					strImporte = filtroImporte;
					Coutas couta = new Coutas(strNro,strMoneda,strImporte);
					if(existsinArray(arrayCoutas, strNro)) {
						ultimoencontrado = true;
						System.out.println("Se encontro el ultimo registro...");
					}else {
						arrayCoutas.add(couta);
						String rutaExcel = "C:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-OutputCoutas.xlsx";
						int cellExcel = Excel.getNumLastRowByBook(rutaExcel) + 1;
						String nroCorrelativo = Excel.obtenerValorCelda(rutaExcel, cellExcel - 1,0);
						if(nroCorrelativo.equals("nroCorrelativo")) {
							nroCorrelativo = "0";
						}
						Excel.establecerValorCelda(rutaExcel, cellExcel, 0, String.valueOf(Integer.parseInt(nroCorrelativo)+1));
						Excel.establecerValorCelda(rutaExcel,cellExcel, 1, cuenta);
						Excel.establecerValorCelda(rutaExcel,cellExcel, 2, couta.getNro());
						Excel.establecerValorCelda(rutaExcel,cellExcel, 3, couta.getMoneda());
						Excel.establecerValorCelda(rutaExcel,cellExcel, 4, couta.getImporte());
						
						validarExistenciaCoutaReg(couta,cuenta);
		
						if(cuenta.contains("SAT")){
							Calendar c1 = Calendar.getInstance();
							String fecha = c1.get(Calendar.DAY_OF_MONTH) + "-" + c1.get(Calendar.MONTH) + "-" + c1.get(Calendar.YEAR);
							Excel.establecerValorCelda(rutaExcel,cellExcel, 5, fecha);
							Excel.establecerValorCelda(rutaExcel,cellExcel, 6, "otros medios");
						}else {
							Excel.establecerValorCelda(rutaExcel,cellExcel, 7, "PAGO COUTAS");
							Excel.establecerValorCelda(rutaExcel,cellExcel, 8, "IB");
						}
					}
				}else {
					System.out.println("Se encontro el ultimo registro...");
					ultimoencontrado = true;
					break;
				}
			}
			if(ultimoencontrado) {
				break;
			}
			
			//Segunda barrida de registros
			for(int index=0;index<numberRows;index++) {
				Thread.sleep(300);
				auto.getAutoIt().send("{DOWN}",false);
			}
			for(int timesClick=0;timesClick<9;timesClick++) {
				Thread.sleep(500);
				auto.getAutoIt().send("+{TAB}",false);
			}
			pasadas++;
		}
		System.out.println("***Metodo Terminado Coutas***");
		//CodigoFinanciera2
		this.ui.lblFinancieraTwoTitle.click();
		Thread.sleep(1000);
	}
	public String  captureValueCell(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio +2, element.yInicio +1 +(x * element.alturaCelda),
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	public String  captureValueCellOnlyNumber(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio + 2, element.yInicio + (x * element.alturaCelda),
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImageOnlyNumber(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	public String  captureValueCellOnlyLetters(Sikulix element, int x) throws AWTException, IOException, InterruptedException {
		String ruta = element.captureCell(element.xInicio +2, element.yInicio + (x * element.alturaCelda),
				element.longitudCelda ,element.alturaCelda, 1);
		Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
		String strNro = tesseractOCR.getTextImageOnlyLetters(ruta.substring(0,ruta.length()-4) + "_copia.png");
		return strNro;
	}
	
	public boolean existsinArray(ArrayList<Coutas> array, String nro) {
		for(Coutas couta: array) {
			if(couta.getNro().contains(nro.trim())){
				return true;
			}
		}
		return false;
	}
	public boolean existsinArrayPagos(ArrayList<Pagos> arrayPagos, String operacion) {
		for(Pagos pago: arrayPagos) {
			if(pago.getOperacion().contains(operacion.trim())){
				return true;
			}
		}
		return false;
	}
	public void validarHistoricoPagos(int numberRows) throws InterruptedException, FindFailed, AWTException, IOException {
		boolean ultimoencontrado=false;
		int pasadas = 0;
		ArrayList<Pagos> arrayPagos = new ArrayList<Pagos>();
		while(!ultimoencontrado) {
			auto.getAutoIt().send("{TAB}",false);
			Thread.sleep(100);
			auto.getAutoIt().send("+{END}",false);
			Thread.sleep(100);
			auto.getAutoIt().send("^c",false);
			String cuenta = auto.getAutoIt().clipGet();
			auto.getAutoIt().send("{TAB}",false);
			Thread.sleep(100);
			auto.getAutoIt().send("+{END}",false);
			Thread.sleep(100);	
			auto.getAutoIt().send("^c",false);
			cuenta += auto.getAutoIt().clipGet();
			this.ui.lblOperacionTable.captureSizeElement();
			this.ui.lblFechaPagoTable.captureSizeElement();
			this.ui.lblMtoPagoTable.captureSizeElement();
			this.ui.lblTipPagoTable.captureSizeElement();
			for(int timesClick=0;timesClick<8;timesClick++) {
				Thread.sleep(500);
				auto.getAutoIt().send("{TAB}",false);
			}
			//Primera barrida de registros
			for(int index=0;index<numberRows;index++) {
				Thread.sleep(300);
				auto.getAutoIt().send("{DOWN}",false);
			}
			for(int x=1;x<numberRows+1;x++) {
				String strFechaPago = captureValueCell(this.ui.lblFechaPagoTable, x);
				String strMtoPago = captureValueCell(this.ui.lblMtoPagoTable, x);
				String strTipoPago = captureValueCellOnlyLetters(this.ui.lblTipPagoTable, x);
				String strOperacion = captureValueCellOnlyNumber(this.ui.lblOperacionTable, x);
				if(!strFechaPago.trim().equals("") && !strMtoPago.trim().equals("") && !strTipoPago.trim().equals("") && !strOperacion.trim().equals("")) {
					String ultimocharTipoPago = strMtoPago.substring(strMtoPago.length()-1, strMtoPago.length());
					if(!Character.isDigit(ultimocharTipoPago.charAt(0))){
						strMtoPago = strMtoPago.substring(0, strMtoPago.length()-1);
					}
					String ultimocharFecha = strFechaPago.substring(strFechaPago.length()-1, strFechaPago.length());
					if(!Character.isDigit(ultimocharFecha.charAt(0))){
						strFechaPago = strFechaPago.substring(0, strFechaPago.length()-1);
					}
					if(strTipoPago.contains("PAG0 T")) {
						strTipoPago = "Pago Tarjeta Credito";
					}else if (strTipoPago.contains("AB0N0")){
						strTipoPago = "Abono Regular";
					}else if(strTipoPago.contains("PAG0DEP0SITC")) {
						strTipoPago = "Pago Deposito";
					}else if(strTipoPago.contains("PAG0P0RREPR")) {
						strTipoPago = "Pago por Reprog";
					}else if(strTipoPago.contains("PAG0DEP0SITC")) {
						strTipoPago = "Pago Deposito";
					}else if(strTipoPago.contains("PAG0DECU0")) {
						strTipoPago = "Pago de Couta";
					}else if(strTipoPago.contains("VENT")) {
						strTipoPago = "Pago Tarjeta Venta";
					}
					Pagos pago = new Pagos(strOperacion,strFechaPago,strMtoPago,strTipoPago);
					if(existsinArrayPagos(arrayPagos, strOperacion)) {
						ultimoencontrado = true;
						System.out.println("Se encontro el ultimo registro...");
					}else {
						arrayPagos.add(pago);
						String rutaExcel = "C:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-OutputPagos.xlsx";
						int cellExcel = Excel.getNumLastRowByBook(rutaExcel) + 1;
						Excel.establecerValorCelda(rutaExcel,cellExcel, 0, cuenta);
						Excel.establecerValorCelda(rutaExcel,cellExcel, 1, pago.getOperacion());
						Excel.establecerValorCelda(rutaExcel,cellExcel, 2, pago.getFechaPago());
						Excel.establecerValorCelda(rutaExcel,cellExcel, 3, pago.getMtoPago());
						Excel.establecerValorCelda(rutaExcel,cellExcel, 4, pago.getTipPago());
						validarExistenciaPagoReg(pago, cuenta);
					}
				}else {
					ultimoencontrado = true;
					System.out.println("Se encontro el ultimo registro...");
					break;
				}
			}
			if(ultimoencontrado) {
				break;
			}
			
			//Segunda barrida de registros
			for(int index=0;index<numberRows;index++) {
				Thread.sleep(300);
				auto.getAutoIt().send("{DOWN}",false);
			}
			this.ui.lblFinancieraTwoTitle.click();
			pasadas++;
		}
		System.out.println("***Metodo Terminado Pagos***");
	}
	public void validarExistenciaCoutaReg(Coutas couta, String cuenta) {
		try {
			properties.load(new FileReader("properties\\general.properties"));
			File archivo = new File(properties.getProperty("coutas"));
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			String linea = "";
			while((linea=br.readLine())!=null) {
				if(linea.contains(cuenta) && linea.contains(couta.getImporte())) {
					System.out.println("Cuenta en REG 100");
					System.out.println(linea);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void validarExistenciaPagoReg(Pagos pago,String cuenta) {
		try {
			properties.load(new FileReader("properties\\general.properties"));
			File archivo = new File(properties.getProperty("pagos"));
			FileReader fr = new FileReader(archivo);
			BufferedReader br = new BufferedReader(fr);
			String linea = "";
			while((linea=br.readLine())!=null) {
				if(linea.contains(cuenta) && linea.contains(pago.getMtoPago())) {
					System.out.println("Cuenta en REG 500");
					System.out.println(linea);
				}
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
