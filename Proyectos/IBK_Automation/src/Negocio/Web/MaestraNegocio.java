package Negocio.Web;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import org.sikuli.script.FindFailed;

import Utilitario.Conecci�n.AutoIT_Clipboard;
import Base.Selenium;

public class MaestraNegocio{

	// DECLARACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	private Selenium accion;
	
	// INICIALIZACION DE VARIABLES DEL WEBDRIVER Y WINIUMDRIVER
	public MaestraNegocio() {
		accion = new Selenium();
	}

	// PASOS DE PRUEBAS
	public String ValidarGrupo(String MGrupo) throws FindFailed, InterruptedException, ClassNotFoundException, UnsupportedFlavorException, IOException {
		//Thread.sleep(500);
		accion.ClickAImagen("Resources\\Ubicacion.png");
		AutoIT_Clipboard svrs = new AutoIT_Clipboard();
		Thread.sleep(500);
		svrs.getAutoIt().send("{TAB}",false);
		Thread.sleep(500);
		svrs.getAutoIt().send("{HOME}",false);
	    svrs.getAutoIt().send("+{END}",false);
		//Copiar
		svrs.getAutoIt().send("^c",false);
	    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable t = cb.getContents(this);
		// Construimos el DataFlavor correspondiente al String java
		
		DataFlavor dataFlavorStringJava = new DataFlavor("application/x-java-serialized-object; class=java.lang.String");
		MGrupo = (String) t.getTransferData(dataFlavorStringJava);
		System.out.println("Grupo a validar: " + MGrupo);
		return MGrupo;
	}
	
	public String ValidarCuenta(String MCuenta1) throws FindFailed, InterruptedException, ClassNotFoundException, UnsupportedFlavorException, IOException {
		AutoIT_Clipboard svrs = new AutoIT_Clipboard();
		svrs.getAutoIt().send("{TAB}",false);
		svrs.getAutoIt().send("{HOME}",false);
	    svrs.getAutoIt().send("+{END}",false);
		//Copiar
		svrs.getAutoIt().send("^c",false);
	    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable t = cb.getContents(this);
		// Construimos el DataFlavor correspondiente al String java
		
		DataFlavor dataFlavorStringJava = new DataFlavor("application/x-java-serialized-object; class=java.lang.String");
		MCuenta1 = (String) t.getTransferData(dataFlavorStringJava);
		System.out.println("Cuenta a validar: " + MCuenta1);
		return MCuenta1;

	}
	
	public String ValidarMontoVencido(String MMontoVencido) throws FindFailed, InterruptedException, ClassNotFoundException, UnsupportedFlavorException, IOException {
		AutoIT_Clipboard svrs = new AutoIT_Clipboard();
		
		for(int i = 0; i<28; i++) {
			svrs.getAutoIt().send("{TAB}",false);
		}
		svrs.getAutoIt().send("{HOME}",false);
	    svrs.getAutoIt().send("+{END}",false);
		//Copiar
		svrs.getAutoIt().send("^c",false);
	    Clipboard cb = Toolkit.getDefaultToolkit().getSystemClipboard();
		Transferable t = cb.getContents(this);
		// Construimos el DataFlavor correspondiente al String java
		
		DataFlavor dataFlavorStringJava = new DataFlavor("application/x-java-serialized-object; class=java.lang.String");
		MMontoVencido = (String) t.getTransferData(dataFlavorStringJava);
		System.out.println("Monto Vencido a validar: " + MMontoVencido);
		return MMontoVencido;

	}

	
	
	
}
