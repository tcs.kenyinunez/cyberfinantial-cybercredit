package Negocio.Web;

import java.awt.AWTException;
import java.io.IOException;

import org.sikuli.script.FindFailed;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import interfazUsuario.AgregarTelefonoDireccionUi;
import parametros.TpCyberCreditAgreTelDireccion;

public class AgregarTelefonoDireccionNegocio {
	private AutoIt auto = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	public AgregarTelefonoDireccionUi ui = new AgregarTelefonoDireccionUi();
	
	public void agregarTelefono(TpCyberCreditAgreTelDireccion caso) throws Exception {
		String area = "000";
		String numero = "987654212";
		String extension = "987";
		Thread.sleep(1000);
		this.ui.lblTel.click();
		Thread.sleep(1000);
		this.ui.btnAgregar.click();
		if(!caso.getTipo().equalsIgnoreCase(" ")) {
			this.ui.cmbTipo.click();
			switch(caso.getTipo()) {
				case "Casa":
				case "casa":{
					auto.getAutoIt().send("{ENTER}", false);
					break;
				}
				case "Celular":
				case "celular":{
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{ENTER}", false);
					break;
				}
				case "Familiar":
				case "familiar":{
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{ENTER}", false);
					break;
				}
				case "Oficina":
				case "oficina":{
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{ENTER}", false);
					break;
				}
				case "Otra":
				case "otra":{
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{DOWN}", false);
					Thread.sleep(500);
					auto.getAutoIt().send("{ENTER}", false);
					break;
				}
				default:{
					throw new Exception("No se ha identificado el tipo de telefono : " + caso.getTipo());
				}
			}
			this.ui.txtCodArea.sendKeys(area);
			this.ui.txtTelefonoAgregarTelefono.clearText();
			this.ui.txtTelefonoAgregarTelefono.sendKeys(numero);
			this.ui.txtExtensionAgregarTelefono.sendKeys(extension);
			this.ui.btnAceptarAgregarTelefono.click();
			Thread.sleep(1000);
			this.ui.btnOk.waitAtImage();
			this.ui.btnOk.click();
		}else {
			System.out.println("No se ha agregado telefonos");
		}
		this.ui.cabeceraTelefono.captureSizeElement();
		String ultimomensaje = "";
		for(int times=1;times<28;times++) {
			String ruta = this.ui.cabeceraTelefono.captureCell(this.ui.cabeceraTelefono.xInicio + (this.ui.cabeceraTelefono.longitudCelda * 60/100), this.ui.cabeceraTelefono.yInicio + (times* (this.ui.cabeceraTelefono.alturaCelda-2)),
					(this.ui.cabeceraTelefono.longitudCelda *40/100)  + this.ui.cabeceraTelefono.alturaCelda, this.ui.cabeceraTelefono.alturaCelda, times);
			Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
			String celda1 = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
			String validar = "(" + area + ")" + numero + "ext.:" + extension;
			if(!celda1.equals("")) {
				if(validar.equals(celda1)) {
					System.out.println("Validado");
					break;
				}else {
					System.out.println("Incorrecto");
				}
			}else {
				throw new Exception("No se encontro el telefono ingresado");
			}
		}
		
	}
	public void agregarDireccion(TpCyberCreditAgreTelDireccion caso) throws Exception {
		Thread.sleep(1000);
		this.ui.lblDireccion.waitAtImage();
		this.ui.lblDireccion.click();
		Thread.sleep(1000);
		this.ui.btnAgregar.click();
		Thread.sleep(1000);
		this.ui.cmbTipoDireccion.click();
		switch(caso.getTipoDireccion()) {
		case "Casa":
		case "casa":{
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "Cobranzas":
		case "cobranzas":{
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "Correo Electronico":
		case "correo electronico":{
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "Familiar":
		case "familiar":{
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "Oficina":
		case "oficina":{
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{DOWN}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		case "Otra":
		case "otra":{
			auto.getAutoIt().send("{END}",false);
			Thread.sleep(1000);
			auto.getAutoIt().send("{ENTER}",false);
			break;
		}
		default: {
			throw new Exception("No se reconocio el tipo de direccion " + caso.getTipoDireccion());
		}
		}
		Thread.sleep(1000);
		this.ui.txtDireccion.sendKeys(caso.getDireccion());
		Thread.sleep(1000);
		this.ui.txtDistrito.sendKeys(caso.getDistrito());
		this.ui.txtProvincia.sendKeys(caso.getProvincia());
		Thread.sleep(1000);
		this.ui.txtDepartamentoCorto.sendKeys(caso.getDepartamentoCorto());
		this.ui.txtCP.sendKeys(caso.getCp());
		Thread.sleep(1000);
		this.ui.btnLocalizar.click();
		Thread.sleep(1000);
		this.ui.btnOk.click();
		Thread.sleep(2000);
		this.ui.btnAceptarAgregarTelefono.click();
		if (this.ui.popUpExito.exists()) {
			this.ui.btnOk.click();
		}else {
			throw new Exception("Hubo un error al ingresar la direccion");
		}
	}
}
