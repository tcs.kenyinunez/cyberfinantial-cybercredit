package Pruebas.Web;

import org.testng.annotations.Test;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Negocio.Web.ComprobarStatusNegocio;
import Negocio.Web.LoginCyberCreditNegocio;
import Utilitario.Conecci�n.InitSelenium;
import parametros.TpComprobarStatus;
import parametros.TpPagoCouta;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.testng.annotations.AfterClass;

public class ComprobarStatusPrueba {
	Selenium selenium = new Selenium();
	private ComprobarStatusNegocio negocio = new ComprobarStatusNegocio();
	private LoginCyberCreditNegocio loginPage = null;
	private Properties properties = new Properties();
	private Sikulix sikulix = new Sikulix("");
	private AutoIt auto = new AutoIt();
	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {
	
	}
	
	@DataProvider
	public Object[][] dp() throws IOException {
		Object[][] arrayObject = null;
		ArrayList<TpComprobarStatus> arrayExcel = null;
		try {
		arrayExcel = TpComprobarStatus.leerExcel();
		int i = 0;
		arrayObject = new Object[arrayExcel.size()][2];
		for(TpComprobarStatus caso: arrayExcel) {
    		arrayObject[i][0] = i;
    		arrayObject[i][1] = caso;
    		i++;
    		}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return arrayObject;
	}
	
	@Test(dataProvider = "dp")
	public void testMain(Integer n, TpComprobarStatus caso) throws IOException, InterruptedException {
		try {
			
			if(caso.getMensaje().equals("")) {
				properties.load(new FileReader(".\\properties\\general.properties"));
				InitSelenium initSelenium = new InitSelenium();
				initSelenium.IniciarSelenium("internetexplorer");
				Thread.sleep(1000);
				loginPage = new LoginCyberCreditNegocio();
				this.loginPage.ClicCyberCredit();
				this.loginPage.IngresarNomUsuario(properties.getProperty("user"));
				sikulix.Maximizar();
				this.loginPage.IngresarNomContrasena(properties.getProperty("pass"));
				this.loginPage.ClicOK();
				Thread.sleep(1000);
				this.negocio.validarStatusR(caso);
				TpComprobarStatus.establecerValorCelda(caso.getNroCorrelativo() + 4, 6, "Exito");
			}
		}catch(Exception e) {
			e.printStackTrace();
			TpComprobarStatus.establecerValorCelda(caso.getNroCorrelativo() + 4, 6, e.getMessage());
		}finally {
			InitSelenium.FinalizarSelenium();
		}
	}
	
	@AfterClass
	public void afterClass() {
	  
	}

}
