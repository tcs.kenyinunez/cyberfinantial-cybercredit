package Pruebas.Web;

import org.testng.annotations.Test;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import Negocio.Web.BitacoraGestionNegocio;
import Negocio.Web.BusquedaGrupoCuentaNegocio;
import Negocio.Web.DetalleClienteNegocio;
import Negocio.Web.LoginCyberCreditNegocio;
import Utilitario.Conecci�n.InitSelenium;
import Utilitario.Reporte.Reporte;

import parametros.TpCyberCreditPromesasPago;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;

public class CyberCreditPromesasPrueba {
	private Reporte reporte;
	private Selenium accion = new Selenium();
	private LoginCyberCreditNegocio loginPage;
	InitSelenium initSelenium = new InitSelenium();
	private BusquedaGrupoCuentaNegocio busquedaGrupoCuentaPage = new BusquedaGrupoCuentaNegocio();
	private DetalleClienteNegocio detalleCliente = new DetalleClienteNegocio();
	private BitacoraGestionNegocio bitacoraGestion = new BitacoraGestionNegocio();
	public AutoIt autoIt = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	public Properties properties = new Properties();
	@BeforeClass
	public void beforeClass() {
		
	}

	@DataProvider(name="Input")
	public Object[][] dp() throws IOException {
		 Object[][] arrayObject = null;
		 ArrayList<TpCyberCreditPromesasPago> arrayExcel = null;
		 arrayExcel = TpCyberCreditPromesasPago.leerExcel();
		 arrayObject = new Object[arrayExcel.size()][2];
		 int i = 0;
	    	for(TpCyberCreditPromesasPago caso: arrayExcel) {
	    		arrayObject[i][0] = String.valueOf(i);
	    		arrayObject[i][1] = caso;
	    		i++;
	    	}
		return arrayObject;
	}
	
	@Test(dataProvider = "Input")
	public void testMain(String index, TpCyberCreditPromesasPago caso) throws IOException, FindFailed, InterruptedException {
		try {
			if(caso.getMensaje()==null || caso.getMensaje().equals("")) {
				properties.load(new FileReader("properties/general.properties"));
				initSelenium.IniciarSelenium("internetexplorer");
				loginPage = new LoginCyberCreditNegocio();
				loginPage.Login(properties.get("user").toString(), properties.get("pass").toString());
				autoIt.Maximizar();
				busquedaGrupoCuentaPage.BuscarCliente(caso.getCliente());
				loginPage.ClicOK();
				Sikulix lblCuenta = new Sikulix("\\Resources\\lblCuentaTable.PNG");
				Sikulix lblProduct = new Sikulix("\\Resources\\lblProductTable.PNG");
				lblCuenta.resizeElement(20);
				lblCuenta.captureSizeElement();
				for(int times = 1; times<3;times++) {
					String ruta = lblCuenta.captureCell(lblCuenta.xInicio, lblCuenta.yInicio + (times*lblCuenta.alturaCelda), lblCuenta.longitudCelda + lblCuenta.alturaCelda,lblCuenta.alturaCelda ,times);
					Selenium.copyImage(ruta, ruta.substring(0,ruta.length()-4) + "_copia.png");
					String celda1 = tesseractOCR.getTextImage(ruta.substring(0,ruta.length()-4) + "_copia.png");
					if (celda1.equals(caso.getCuenta())) {
						Thread.sleep(1000);
						autoIt.getAutoIt().send("{TAB}", false);
						Thread.sleep(1000);
						autoIt.getAutoIt().send("+{TAB}", false);
						Thread.sleep(1000);
						autoIt.getAutoIt().send("{DOWN}",false);
						Thread.sleep(1000);
						autoIt.getAutoIt().send("{ENTER}", false);
						Thread.sleep(1000);
						break;
					}
				}
				detalleCliente.crearPromesaPago(caso);
				TpCyberCreditPromesasPago.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, bitacoraGestion.validarBitacora());
			}
		}catch(Exception e) {
			e.printStackTrace();
			TpCyberCreditPromesasPago.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, e.getMessage());
		}	
	}

	@AfterClass
	public void afterClass() {
		InitSelenium.getDriver().quit();
	}

}
