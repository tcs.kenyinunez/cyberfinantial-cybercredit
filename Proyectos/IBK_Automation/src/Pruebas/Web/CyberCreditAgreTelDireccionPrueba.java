package Pruebas.Web;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;

import Base.AutoIt;
import Base.Selenium;
import Base.TesseractOCR;
import Negocio.Web.AgregarTelefonoDireccionNegocio;
import Negocio.Web.BitacoraGestionNegocio;
import Negocio.Web.BusquedaGrupoCuentaNegocio;
import Negocio.Web.DetalleClienteNegocio;
import Negocio.Web.LoginCyberCreditNegocio;
import Utilitario.Conecci�n.InitSelenium;

import Utilitario.Reporte.*;
import parametros.TpCyberCreditAgreTelDireccion;
import parametros.TpCyberCreditPromesasPago;

public class CyberCreditAgreTelDireccionPrueba {
	private Reporte reporte;
	
	private String rutaCargaDataInput = "";
	private int columnaInicioDatos = 6;
	private int cantidadColumnas = 4;
	
	private Selenium accion = new Selenium();
	private LoginCyberCreditNegocio loginPage;
	InitSelenium initSelenium = new InitSelenium();
	private BusquedaGrupoCuentaNegocio busquedaGrupoCuentaPage = new BusquedaGrupoCuentaNegocio();
	private DetalleClienteNegocio detalleCliente = new DetalleClienteNegocio();
	private AgregarTelefonoDireccionNegocio agregarTelefono = new AgregarTelefonoDireccionNegocio();
	AutoIt autoIt = new AutoIt();
	private TesseractOCR tesseractOCR = new TesseractOCR();
	
	@DataProvider(name="Input")
	public Object[][] LeerExcel() throws IOException{
		Object[][] arrayObject = null;
		 ArrayList<TpCyberCreditAgreTelDireccion> arrayExcel = null;
		 arrayExcel = TpCyberCreditAgreTelDireccion.leerExcel();
		 arrayObject = new Object[arrayExcel.size()][2];
		 int i = 0;
	    	for(TpCyberCreditAgreTelDireccion caso: arrayExcel) {
	    		arrayObject[i][0] = String.valueOf(i);
	    		arrayObject[i][1] = caso;
	    		i++;
	    	}
		return arrayObject;
	}
	
	@Test(dataProvider = "Input")
	public void testMain(String index, TpCyberCreditAgreTelDireccion caso) throws InterruptedException {
		try {
			initSelenium.IniciarSelenium("internetexplorer");
			loginPage = new LoginCyberCreditNegocio();
			loginPage.Login("XT9196", "Interbank4");
			autoIt.Maximizar();
			busquedaGrupoCuentaPage.BuscarCliente(caso.getCliente());
			loginPage.ClicOK();
			detalleCliente.buscarPorCredito();
			agregarTelefono.agregarTelefono(caso);
			if(!caso.getTipoDireccion().equalsIgnoreCase("")) {
				agregarTelefono.agregarDireccion(caso);
			}else {
				System.out.println("No se detecto direccion ha ingresar...");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			InitSelenium.FinalizarSelenium();
		}
	}
	@AfterClass
	public void afterClass() {
	  
	}
}
