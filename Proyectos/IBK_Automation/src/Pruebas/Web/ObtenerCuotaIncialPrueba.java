package Pruebas.Web;

import org.testng.annotations.Test;

import com.mongodb.annotations.ThreadSafe;

import Base.AutoIt;
import Base.Selenium;
import Base.Sikulix;
import Base.TesseractOCR;
import Utilitario.Reporte.*;
import parametros.TpObtenerCoutaInicial;
import parametros.TpPagoCouta;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import Utilitario.Conecci�n.InitSelenium;
import Negocio.Web.*;

public class ObtenerCuotaIncialPrueba {

// 	VARIABLES UTILITARIAS
	private static WebDriver Driver;
	private Selenium accion;
	
	public static WebDriver getDriver(){
        return Driver;
    }

// 	VARIABLES DEL REPORTE
	Reporte reporte;
	String columnaCodigoRelacion = "J"; // Columna excel donde se escribir� el codigo de relaci�n obtenido
	String columnaEstadoEjecucion = "K"; // Columna excel donde se escribir� el estado del escenario ejecutado
	String columnaMensajeObtenido = "L"; // Columna excel donde se escribir� el mensaje obtenido
	String nombreSistema = "CyberFinantial";
	String nombreProceso = "Creditos";
	int contador = 0;

	public String filePath;
	public String copyPath;
	
	public String filePathCuota;
	public String copyPathCuota;
	
	public String filePathPago;
	public String copyPathPago;
	
	public String Cuota;

// 	VARIABLES DEL EXCEL
	int inicioDatos = 6; // Indicador de fila donde comienza el ingreso de datos
	int posicionFila = inicioDatos - 2; // Indicador de fila donde comienza el ingreso de datos
	int cantidadColumnas = 4; // Cantidad de columnas de mi data de entrada
	String rutaCargaDataInput = "c:\\Automatizacion\\Inputs\\CyberFinantial\\CyberFinantial-Creditos.xls";

// 	VALIDADOR
	public boolean ITextError = false;

//	LISTA DE CASOS DE PRUEBAS CON SUS IMAGENES
	List<RegistroFotos> Registros = new ArrayList<RegistroFotos>();

//  CONSTRUCTOR DE LA CLASE QUE INSTANCIA LOS DATOS COMPLEMENTARIOS DEL REPORTE
	public ObtenerCuotaIncialPrueba() {
		reporte = new Reporte(columnaEstadoEjecucion, columnaMensajeObtenido, columnaCodigoRelacion, rutaCargaDataInput,
				nombreSistema, nombreProceso);
	}

// 	INSTANCIA DEL EXCEL
	@DataProvider(name = "Input")
	public Object[][] LeerExcel2() throws IOException {
		Object[][] arrayObject = null;
		ArrayList<TpObtenerCoutaInicial> arrayExcel = null;
		arrayExcel = TpObtenerCoutaInicial.leerExcel();
		arrayObject = new Object[arrayExcel.size()][2];
		int i = 0;
	    	for(TpObtenerCoutaInicial caso: arrayExcel) {
	    		arrayObject[i][0] = String.valueOf(i);
	    		arrayObject[i][1] = caso;
	    		i++;
	    	}
		return arrayObject;
	}

	@Test(dataProvider = "Input")
	public void BusquedaTest01(String index, TpObtenerCoutaInicial caso) throws Exception {

		// INSTANCIA DE LAS IMAGENES
		List<Imagen> imagenes = new ArrayList<Imagen>();
		RegistroFotos objregistro = new RegistroFotos();

		// VARIABLES GLOBALES
		String ValorGlobal1 = "";
		String mensajeError = "";

		try {
			
			reporte.contadorRegistrosAumentar();
			posicionFila++;
			contador = contador + 1;
			reporte.IniciarReporte(caso.getNroCorrelativo() + "-" + caso.getCuenta());
			reporte.LimpiarDatosPagoNoUbicadoSaldoMinimo(posicionFila);

			String rutaReporteEjecucion = "C:\\Automatizacion\\Reportes\\CyberFinantial\\Creditos\\";

			int numRegistro;
			numRegistro = contador;
			objregistro.setNumRegistro(numRegistro);

			InitSelenium initSelenium = new InitSelenium();
			initSelenium.IniciarSelenium("internetexplorer");
    		accion = new Selenium();

			/*-----------------INICIO DE LA EJECUCI�N--------------------*/

			// INSTANCIAS DE LAS CLASES BASE
			Selenium selenium = new Selenium();
			Sikulix sikulix = new Sikulix(rutaReporteEjecucion);
			AutoIt autoIt = new AutoIt();
			TesseractOCR tesseractOCR = new TesseractOCR();
			
			// INSTANCIAS DE LAS CLASES PAGE
			LoginCyberCreditNegocio loginPage = new LoginCyberCreditNegocio();
			BusquedaGrupoCuentaNegocio busquedaGrupoCuentaPage = new BusquedaGrupoCuentaNegocio();
			DetalleClienteNegocio detalleClientePage = new DetalleClienteNegocio();
			MaestraNegocio maestraPage = new MaestraNegocio();
			DemograficaNegocio demograficaPage = new DemograficaNegocio();
			Financiera1Negocio financiera1Page = new Financiera1Negocio();
			Financiera2Negocio financiera2Page = new Financiera2Negocio();
			CierrePageNegocio cierrePage = new CierrePageNegocio();

			// PASOS DE PRUEBAS

			// Logeo al m�dulo CyberCredit
			loginPage.ClicCyberCredit();
			autoIt.getAutoIt().winWait("http://s417va7:9080/bei010Web/bei010/bei10.jsp - Internet Explorer");
			autoIt.getAutoIt().winActivate("http://s417va7:9080/bei010Web/bei010/bei10.jsp - Internet Explorer");
			loginPage.IngresarNomUsuario("xt9196");
			sikulix.Maximizar();
			loginPage.IngresarNomContrasena("Interbank4");
			loginPage.ClicOK();
			
  		    // Busqueda por n�mero de cuenta
			busquedaGrupoCuentaPage.ClicBusquedaGrupoCuenta();
			busquedaGrupoCuentaPage.IngresoBusquedaGrupoCuenta(caso.getCuenta());
			busquedaGrupoCuentaPage.BuscarBusquedaGrupoCuenta();
			loginPage.ClicOK();

			// Validar Cuentas del Cliente
			if(!caso.getSubcuenta().equals("")) {
				Thread.sleep(1000);
				DetalleClienteNegocio detalleCliente = new DetalleClienteNegocio();
				detalleCliente.ui.getLblCuenta().resizeElement(20);
				Thread.sleep(1000);
				Imagen objimagen1 = new Imagen();
				objimagen1.setNombrePrueba("Consultar Detalle del Cliente");
				objimagen1.setRuta(reporte.CapturarEvidencia("VerificarDatos"));
	    		imagenes.add(objimagen1);
				Thread.sleep(1000);
				Imagen objimagen2 = new Imagen();
				objimagen2.setRuta(reporte.CapturarEvidenciaCuenta("VerificarCuenta"));
				String capturaEvidenciaCuenta = reporte.nombreCompletoImagenCuenta;
				String outputpng = "output.png";
				String capturaEvidenciaCuentaPNG = capturaEvidenciaCuenta.replace(".jpg", outputpng);
				filePath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaCuenta;
				copyPath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaCuentaPNG;
				selenium.copyImage(filePath, copyPath);
				tesseractOCR.Cuenta(copyPath);
				detalleClientePage.main(filePath);
			}
			// Validar datos de la pesta�a Maestra
			Thread.sleep(2000);
			autoIt.getAutoIt().send("{ENTER}",false);
			String MGrupo = maestraPage.ValidarGrupo(caso.getGrupo());
			String MCuenta1 = maestraPage.ValidarCuenta(caso.getCuenta());
			String MMontoVencido = maestraPage.ValidarMontoVencido(caso.getMontoVencido());
			if (MGrupo.equals(caso.getGrupo())) {
				System.out.println("Grupo OK");
				if (MCuenta1.equals(caso.getCuenta())) {
					System.out.println("Cuenta OK");
					if (MMontoVencido.equals(caso.getMontoVencido())) {
					System.out.println("Monto Vencido OK");
					}
				} 
			} 
			Thread.sleep(2000);
			Imagen objimagen4 = new Imagen();
			objimagen4.setRuta(reporte.CapturarEvidenciaEstado("VerificarEstado0"));
			String capturaEvidenciaEstado = reporte.estado;
			String outputEstadopng = "output.png";
			String capturaEvidenciaEstadoPNG = capturaEvidenciaEstado.replace(".jpg", outputEstadopng);
			filePath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstado;
			copyPath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstadoPNG;
			selenium.copyImage(filePath, copyPath);
			tesseractOCR.Estado(copyPath);
			
			Thread.sleep(2000);
			demograficaPage.ClicDemografica();
			Imagen objimagen5 = new Imagen();
			objimagen5.setRuta(reporte.CapturarEvidenciaEstado("VerificarEstado1"));
			String capturaEvidenciaEstadoD = reporte.estado;
			String outputEstadopngD = "output.png";
			String capturaEvidenciaEstadoPNGD = capturaEvidenciaEstadoD.replace(".jpg", outputEstadopngD);
			filePath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstadoD;
			copyPath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstadoPNGD;
			selenium.copyImage(filePath, copyPath);
			tesseractOCR.Estado(copyPath);

			Thread.sleep(2000);
			financiera1Page.ClicFinanciera1();
			Imagen objimagen6 = new Imagen();
			objimagen6.setRuta(reporte.CapturarEvidenciaEstado("VerificarEstado2"));
			String capturaEvidenciaEstado1 = reporte.estado;
			String outputEstadopng1 = "output.png";
			String capturaEvidenciaEstadoPNG1 = capturaEvidenciaEstado1.replace(".jpg", outputEstadopng1);
			filePath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstado1;
			copyPath = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaEstadoPNG1;
			selenium.copyImage(filePath, copyPath);
			tesseractOCR.Estado(copyPath);
			
			// Validar datos de la pesta�a Financiera2
			financiera2Page.ClicFinanciera2();
			Thread.sleep(2000);
			autoIt.getAutoIt().send("{ENTER}",false);
			Imagen objimagen3 = new Imagen();
			objimagen3.setRuta(reporte.CapturarEvidenciaCantCuotas("VerificarCuota"));
			String capturaEvidenciaCuota = reporte.numCuotas;
			String outputpngCuota = "output.png";
			String capturaEvidenciaCuotaPNG = capturaEvidenciaCuota.replace(".jpg", outputpngCuota);
			filePathCuota = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaCuota;
			copyPathCuota = rutaReporteEjecucion + caso.getNroCorrelativo() + "-" + caso.getCuenta() + "\\" + capturaEvidenciaCuotaPNG;
			selenium.copyImage(filePathCuota, copyPathCuota);
			Financiera2Negocio f2 = new Financiera2Negocio();
			if(caso.getGrupo().equals("6")) {
				f2.validarHistoricoCuotas(6);
				f2.validarHistoricoPagos(23);
			}else {
				f2.validarHistoricoCuotas(13);
				f2.validarHistoricoPagos(13);
			}
			Thread.sleep(1000);
			detalleClientePage.ui.getTxtCA().sendKeys("Xx");
			Thread.sleep(500);

			cierrePage.ClicCierreVentana();
			
			Thread.sleep(1000);
			autoIt.getAutoIt().send("{ENTER}",false);
//			cierrePage.ClicOKCierre();
//			cierrePage.ClicCierreVentanaCyber();

			String resultadoEjecucion = reporte.columnaEstadoEjecucion;
//
			objregistro.setResultadoEjecucion(resultadoEjecucion);
			objregistro.resultadoEjecucion = "Satisfactorio";
			objregistro.mensaje = "Prueba realizada Satisfactoriamente";
			objregistro.setImagenes(imagenes);// Agrega la lista de imagenes
			Registros.add(objregistro);

			reporte.VerificacionSatisfactoria(posicionFila);

			/*-----------------FIN DE LA EJECUCI�N--------------------*/

		} catch (Exception ex) {
			ex.printStackTrace();
			String codigoRelacion = ValorGlobal1;
			objregistro.setCodigoRelacion(codigoRelacion);
			String resultadoEjecucion = reporte.columnaEstadoEjecucion;
			objregistro.setResultadoEjecucion(resultadoEjecucion);
			if (ex.getMessage() == "Inconcluso") {
				objregistro.mensaje = mensajeError;
				objregistro.resultadoEjecucion = "Inconcluso";
			} else {
				objregistro.mensaje = "Escenario fallido";
				objregistro.resultadoEjecucion = "Fallido";
				reporte.VerificacionFallida(posicionFila, mensajeError);
			}

			objregistro.setImagenes(imagenes);
			Registros.add(objregistro);

			// MATAR DRIVER
			InitSelenium.FinalizarSelenium();
		}
		accion.FinalizarAplicacion();
		InitSelenium.FinalizarSelenium();
	}
	
	
	@BeforeSuite
	public void BeforeSuite() throws IOException, InterruptedException {
		System.out.println("Iniciando robot");
		System.out.println("Cargando dependencias...");
		System.out.println("Leyendo datos de entrada...");
		System.out.println("Iniciando Selenium...");

		// FECHA DE INICIO DEL REPORTE
		reporte.setFechaInicio();
		System.out.println("Inicio de ejecuci�n: " + reporte.getFechaInicio());

	}

	@AfterSuite
	public void AfterSuite() throws IOException, URISyntaxException, InterruptedException {
		System.out.println("Finalizando Selenium");
		System.out.println("Generando Reporte");

		// FECHA FIN DEL REPORTE
		reporte.setFechaFin();
		System.out.println("Fin de ejecuci�n: " + reporte.getFechaFin());

		InitSelenium.FinalizarSelenium();

		// VARIABLES DEL REPORTE
		float pruebasejecutadas = (int) (reporte.contadorRegistros);
		float pruebasok = reporte.contadorSatisfactorio;
		float pruebasfalladas = reporte.contadorFallido;
		float pruebasinconclusas = reporte.contadorInconcluso;
		float porcentajeOK = Math.round((pruebasok / pruebasejecutadas) * 100);
		float porcentajefalladas = Math.round((pruebasfalladas / pruebasejecutadas) * 100);
		float porcentajeinconclusas = Math.round((pruebasinconclusas / pruebasejecutadas) * 100);
		String fechahoraInicio = reporte.getFechaInicio();
		String fechahoraFin = reporte.getFechaFin();
		String rutaReporte = "C:\\Reporte Resumen"; // tiene que estar contenido el dise�o del reporte

		// INSTANCIA DEL REPORTE
		this.crearArchivo_html(pruebasinconclusas, rutaReporte, fechahoraInicio, fechahoraFin, pruebasejecutadas,
				pruebasok, pruebasfalladas, porcentajeOK, porcentajefalladas, porcentajeinconclusas, Registros);
		reporte.ResumenEjecucion();

		// LANZAMIENTO DEL REPORTE
		Desktop.getDesktop().browse(new URI("file:///C:/Reporte%20Resumen/Reporte%20Resumen.html"));
	}

// 	INVOCACI�N DE LAS VARIABLES DEL REPORTE
	public void crearArchivo_html(float pruebasinconclusas, String ruta, String fechahoraInicio, String fechahoraFin,
			float pruebasejecutadas, float pruebasok, float pruebasfalladas, float porcentajeOK,
			float porcentajefalladas, float porcentajeinconclusas, List<RegistroFotos> Registros) {
		reporte.crearArchivo_html(pruebasinconclusas, ruta, fechahoraInicio, fechahoraFin, pruebasejecutadas, pruebasok,
				pruebasfalladas, porcentajeOK, porcentajefalladas, porcentajeinconclusas, Registros);
	}
}