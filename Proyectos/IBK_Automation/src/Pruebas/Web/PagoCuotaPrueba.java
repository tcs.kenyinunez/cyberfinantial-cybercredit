package Pruebas.Web;

import org.testng.annotations.Test;

import Base.AutoIt;
import Base.Selenium;
import Negocio.Web.LoginCyberCreditNegocio;
import Negocio.Web.PagoCoutaNegocio;
import Utilitario.Conecci�n.InitSelenium;
import Utilitario.Reporte.Reporte;
import parametros.TpCyberCreditAgreTelDireccion;
import parametros.TpPagoCouta;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.ArrayList;

import org.testng.annotations.AfterClass;

public class PagoCuotaPrueba {
	private Reporte reporte;
	private Selenium selenium = new Selenium();
	private LoginCyberCreditNegocio loginPage;
	private InitSelenium initSelenium = new InitSelenium();
	private PagoCoutaNegocio negocio = new PagoCoutaNegocio();
	private AutoIt auto = new AutoIt();
	
	@BeforeClass
	public void beforeClass() throws IOException, InterruptedException {
		
	}
	@DataProvider(name="dp")
	public Object[][] dp() throws IOException {
		Object[][] arrayObject = null;
		ArrayList<TpPagoCouta> arrayExcel = null;
		arrayExcel = TpPagoCouta.leerExcel();
		arrayObject = new Object[arrayExcel.size()][2];
		int i = 0;
	    	for(TpPagoCouta caso: arrayExcel) {
	    		arrayObject[i][0] = String.valueOf(i);
	    		arrayObject[i][1] = caso;
	    		i++;
	    	}
		return arrayObject;
	}
	@Test(dataProvider = "dp")
	public void testMain(String index, TpPagoCouta caso) throws IOException {
		try {
			if(caso.getMensaje().equals("")) {
				if(caso.getNroCuenta().contains("SAT")) {
					if(initSelenium.getDriver()==null) {
						initSelenium.IniciarSelenium("internetexplorer");
					}
					Thread.sleep(1000);
					initSelenium.getDriver().get("http://130.30.6.51:9131/sat");
					//Pagina NSAT FOCO
					auto.getAutoIt().winActivate("S.A.T (Interbank) - Internet Explorer");
					this.negocio.login();
					this.negocio.buscarContrato(caso);
				}else if(caso.getNroCuenta().contains("HPC")){
					if(initSelenium.getDriver()==null) {
						initSelenium.IniciarSelenium("internetexplorer2");
					}
					Thread.sleep(1000);
					initSelenium.getDriver().get("http://intranetib/sda/");
					auto.getAutoIt().winActivate("SDA - Internet Explorer");
					this.negocio.pagarCuotaHPC(caso);
				}
			}
		}catch(Exception e) {			
			e.printStackTrace();
			TpPagoCouta.establecerValorCelda(caso.getNroCorrelativo() + 4, 9, e.getMessage());
		}
	}
	@AfterClass
	public void afterClass() throws InterruptedException {
		initSelenium.FinalizarSelenium();
	}
}
